import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import BotBar from '../components/BotBar';
import DashboardNav from '../components/DashboardNav';
import FullLoadingComponent from '../components/FullLoadingComponent';
import LoginBrandSelector from '../components/LoginBrandSelector';
import { GX_API_ENDPOINT } from '../configs';
import { AppContext } from '../contexts/AppContext';
import ChatContext from '../contexts/ChatContext';
import LocalStorageHelper from '../utils/LocalStorageHelper';

const DashboardLayout = ({ children, rightSideBar }) => {
  // const [isReady, setIsReady] = useState(true);

  const history = useHistory();

  const {
    isAdminLoggedIn,
    userFullName,
    profileImg,
    isBrandSwicherOpen,
    setIsBrandSwicherOpen,
    setActiveOperatorApp,
    setActiveBrand,
  } = useContext(AppContext);

  const { setBotActiveCategory, setIsBot } = useContext(ChatContext);

  const [loggedIn, setLoggedIn] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [isChatExpanded, setIsChatExpanded] = useState(false);
  const [userBrandList, setUserBrandList] = useState();

  // useEffect(() => {
  //   const timeout = setTimeout(() => {
  //     setIsReady(false);
  //   }, [2000]);

  //   return () => {
  //     clearTimeout(timeout);
  //   };
  // }, []);

  useEffect(() => {
    // Checking if token is valid
    const token = LocalStorageHelper.getAppToken();
    const email = LocalStorageHelper.getAppEmail();

    if (token) {
      axios
        .post(`${GX_API_ENDPOINT}/brokerage/verify_token`, {
          token,
          userpoolid: 'us-east-2_ALARK1RAa',
        })
        .then((resp) => {
          let user;
          if (resp.data) user = resp.data;
          else user = null;

          if (user !== null && Date.now() < user.exp * 1000) {
            // console.log('Session valid!!');
            setLoggedIn(true);
          } else {
            // console.log('Invalid Session', resp);
            LocalStorageHelper.setAppLoginData('', '', '');
            setLoggedIn(false);
          }
        })
        .catch((error) => {
          // console.log('Token verify Error', error);
          setLoggedIn(false);
        })
        .finally(() => setIsLoading(false));
    } else {
      setLoggedIn(false);
    }

    if (email) {
      axios
        .get(`${GX_API_ENDPOINT}/gxb/app/gxlive/user/operator/get`, {
          params: { email, show_apps: true },
        })
        .then(({ data }) => {
          // console.log('Data', data);
          setUserBrandList(data?.operators || []);
        })
        .catch((error) => {
          console.log('Error on getting brand', error);
        });
    }
  }, []);

  useEffect(() => {
    if (!loggedIn) {
      window.location.replace('/login');
    }
  }, [loggedIn]);

  const onRemoveAdminView = () => {
    LocalStorageHelper.removeAdminView();
    window.location.reload();
  };

  const onChangeUserClick = () => {
    setBotActiveCategory({ title: 'Admin' });
    setIsBot(true);
  };

  const onBrandSelected = (brand, app) => {
    LocalStorageHelper.setLoggedBrandData(brand);
    setIsBrandSwicherOpen(false);
    setActiveBrand(brand);
    if (app) {
      setActiveOperatorApp(app);
    }
  };

  return (
    <>
      <div className="broker-app-wrapper">
        {/* {isLoading && <FullLoadingComponent />} */}
        <div className="broker-app-row">
          <DashboardNav />
          <div className="broker-main-row">
            <div className="d-flex flex-column flex-fill">
              <div className="d-flex flex-fill">{children}</div>
              {isAdminLoggedIn ? (
                <div className="admin-view-wrapper">
                  <div className="header">Currently Viewing As</div>
                  <img src={profileImg} alt="" className="user-img" />
                  <div className="user-name">{userFullName}</div>
                  <div className="filled-btn" onClick={onChangeUserClick}>
                    Change User
                  </div>
                  <div className="outlined-btn" onClick={onRemoveAdminView}>
                    Close User View
                  </div>
                </div>
              ) : null}
            </div>
            {rightSideBar || (
              <BotBar
                toggleChatWidth={() => setIsChatExpanded(!isChatExpanded)}
                isChatExpanded={isChatExpanded}
              />
            )}
          </div>
        </div>
        {isBrandSwicherOpen && (
          <div className="brand-switcher">
            <LoginBrandSelector
              brandsData={userBrandList}
              navigateToDashboard={onBrandSelected}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default DashboardLayout;
