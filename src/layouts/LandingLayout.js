import React, { useEffect, useState } from 'react';
import BottomStick from '../components/BottomStick';
import FullLoadingComponent from '../components/FullLoadingComponent';
import NavbarTop from '../components/NavbarTop';
import SiteHeader from '../components/SiteHeader';

const LandingLayout = ({ children, isLoading }) => {
  const [isReady, setIsReady] = useState(true);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setIsReady(false);
    }, [2000]);

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  return (
    <div className="landing-wrapper">
      {(isReady || isLoading) && <FullLoadingComponent />}
      <SiteHeader />
      {children}
      {/* <BottomStick /> */}
    </div>
  );
};

export default LandingLayout;
