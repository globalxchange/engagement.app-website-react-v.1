import React from 'react';
import SideImage from '../assets/images/registration-side-image.jpg';
import AppLogo from '../assets/images/engagement-app-logo.svg';

const RegistrationLayout = ({ children, sideBarComponent, headerLogo }) => (
  <div className="app-registration">
    <div className="container-fluid flex-fill d-flex flex-column">
      <div className="row flex-fill">
        <div className="col-md-4 px-0 d-flex flex-column">
          {sideBarComponent || (
            <img src={SideImage} alt="" className="side-image" />
          )}
        </div>
        <div className="col-md-8  d-flex flex-column position-relative">
          <div className="registration-container">
            <img src={headerLogo || AppLogo} alt="" className="app-logo" />
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default RegistrationLayout;
