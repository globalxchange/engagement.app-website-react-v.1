import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { GX_API_ENDPOINT, USERPOOL_ID } from '../configs';
import LocalStorageHelper from '../utils/LocalStorageHelper';
import axios from 'axios';

const AuthLayout = ({ children, className }) => {
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    const token = LocalStorageHelper.getAppToken();
    if (token) {
      axios
        .post(`${GX_API_ENDPOINT}/brokerage/verify_token`, {
          token,
          userpoolid: USERPOOL_ID,
        })
        .then(resp => {
          let user;
          if (resp.data) user = resp.data;
          else user = null;

          if (user !== null && Date.now() < user.exp * 1000) {
            // console.log('Session valid!!');
            setRedirect(true);
          }
        })
        .catch(error => {
          // console.log('Token verify Error', error);
        });
    }

    return () => {};
  }, []);

  return (
    <>
      {redirect && <Redirect to="/dashboard" />}
      <div className={`login-page ${className || ''}`}>{children}</div>
    </>
  );
};

export default AuthLayout;
