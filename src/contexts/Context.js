import React, { Component } from 'react';

const ProductContext = React.createContext();

class ProductProvider extends Component {
  state = {
    isMobMenu: false,
  };

  isMobMenuOpen = () => {
    this.setState({ isMobMenu: true });
  };
  isMobMenuClose = () => {
    this.setState({ isMobMenu: false });
  };

  render() {
    return (
      <ProductContext.Provider
        value={{
          ...this.state,

          isMobMenuOpen: this.isMobMenuOpen,
          isMobMenuClose: this.isMobMenuClose,
        }}
      >
        {this.props.children}
      </ProductContext.Provider>
    );
  }
}

const ProductConsumer = ProductContext.Consumer;

export { ProductProvider, ProductConsumer };
