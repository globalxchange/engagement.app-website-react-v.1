/* eslint-disable import/prefer-default-export */
import profileIcon from '../assets/images/SideNavIcons/profile-white.png';
import controllerIcon from '../assets/images/SideNavIcons/controller-white.png';
import crmIcon from '../assets/images/SideNavIcons/crm-white.png';
import marketingIcon from '../assets/images/SideNavIcons/marketing-white.png';
import channelIcon from '../assets/images/SideNavIcons/channel-white.png';
import calenderIcon from '../assets/images/SideNavIcons/calender-white.png';
import chatIcon from '../assets/images/SideNavIcons/chats-white.png';
import profileNavIcon from '../assets/images/SideNavIcons/profile-menu-icon.svg';
import affiliateNavIcon from '../assets/images/SideNavIcons/afflilates-menu-icon.svg';
import compesationNavIcon from '../assets/images/SideNavIcons/compensation-menu-icon.png';
import campaignsNavIcon from '../assets/images/SideNavIcons/campigns-menu-icon.svg';
import chatsNavIcon from '../assets/images/SideNavIcons/influence-icon.svg';
import productsIcon from '../assets/images/SideNavIcons/products-icon.svg';

export const sideBarMenu = [
  {
    menuTitle: 'Profile',
    path: '/',
    icon: profileNavIcon,
    menus: [
      { title: 'PR Studio', path: '#' },
      { title: 'Writters', path: '#' },
      { title: 'Videos', path: '#' },
      { title: 'Images', path: '#' },
    ],
  },
  {
    menuTitle: 'Products',
    path: '/products',
    icon: productsIcon,
    menus: [
      {
        title: 'My Pipeline',
        icon: null,
        path: '/products/mypipeline',
      },
      { title: 'My Products', icon: null },
    ],
  },
  {
    menuTitle: 'Affiliates',
    icon: affiliateNavIcon,
    menus: [
      { title: 'CRM', icon: crmIcon, path: '/dashboard/crm' },
      {
        title: 'Profile',
        icon: profileIcon,
        path: '/dashboard/profile',
        mainMenus: [
          'Overview',
          'Services',
          'Testimonials',
          'Partners',
          'Consulting',
        ],
      },
      {
        title: 'Controller',
        icon: controllerIcon,
        path: '/dashboard/controller',
        mainMenus: ['Main', 'Training', 'Licence', 'OTC'],
      },
      { title: 'Marketing', icon: marketingIcon, disabled: true },
      { title: 'Channels', icon: channelIcon, disabled: true },
      { title: 'Calender', icon: calenderIcon, disabled: true },
      { title: 'Chat', icon: chatIcon, disabled: true },
    ],
  },
  {
    menuTitle: 'Compensation',
    icon: compesationNavIcon,
    path: '/compensation',
    menus: [
      { title: 'CompMarket', path: '/compensation/comp-market' },
      { title: 'CompBuilder', path: '/compensation/creator' },
      { title: 'My CompPlans', path: '/compensation/my-comp-plans' },
      { title: 'My Products', path: '/compensation/myproducts' },
      { title: 'CompTokens', path: '/compensation/cp-roayalties' },
    ],
  },
  {
    menuTitle: 'Campaigns',
    icon: campaignsNavIcon,
    menus: [
      { title: 'Explore', icon: '', disabled: true },
      { title: 'Learn', icon: '', disabled: true },
      { title: 'Brand App', icon: '', disabled: true },
    ],
  },
  {
    menuTitle: 'Influence Chat',
    icon: chatsNavIcon,
    isChatAction: true,
    menus: [],
  },
];

export const GX_API_ENDPOINT = 'https://comms.globalxchange.io';

export const GX_AUTH_URL = 'https://gxauth.apimachine.com';

export const APP_CODE = 'farm';
export const EMPOWERED_APP_CODE = 'EmpoweredApp';
export const AFFILIATE_BANK_APP_CODE = 'snappay';
export const ENGAGEMENT_APP_CODE = 'farm';

export const searchSuggestionListCRM = [
  { title: 'Total Users', count: 200 },
  { title: 'Direct Users', count: 200 },
  { title: 'Indirect Users', count: 200 },
  { title: 'Total Customers', count: 3021 },
  { title: 'Direct Customers', count: 3021 },
  { title: 'Indirect Customers', count: 3021 },
  { title: 'Total Brokers', count: 3021 },
  { title: 'Direct Brokers', count: 3021 },
  { title: 'Indirect Brokers', count: 3021 },
  { title: 'Total Token Transactional Volume', count: 3021 },
  { title: 'Direct Token Transactional Revenue', count: 3021 },
  { title: 'Indirect Token Transactional Revenue', count: 3021 },
  { title: 'Broker Dealer Transactional Revenue', count: 3021 },
  { title: 'Total OTC Transactional Volume', count: 3021 },
  { title: 'Total OTC Transactional Revenue', count: 3021 },
  { title: 'Direct OTC Transactional Revenue', count: 3021 },
  { title: 'Indirect OTC Transactional Revenue', count: 3021 },
  { title: 'Brokerage Money Market Earnings', count: 3021 },
  { title: 'Total Digital Transactional Revenue', count: 3021 },
  { title: 'Direct Digital Transactional Volume', count: 3021 },
  { title: 'Indirect Digital Transactional Volume', count: 3021 },
  { title: 'Broker Dealer Revenue', count: 3021 },
  { title: 'Withdrawal Ledger', count: 3021 },
  {
    title: 'Total OTC Transactional Volume-InstaCryptoPurchase.com',
    count: 3021,
  },
  {
    title: 'Total OTC Transactional Revenue-InstaCryptoPurchase.com',
    count: 3021,
  },
  {
    title: 'Direct OTC Transactional Revenue-InstaCryptoPurchase.com',
    count: 3021,
  },
  {
    title: 'Indirect OTC Transactional Revenue-InstaCryptoPurchase.com',
    count: 3021,
  },
];

export const BOTS_MENU = [
  {
    title: 'Transactions',
    header: 'What Is The Type Of Transaction',
    subMenu: [
      {
        title: 'Add',
        icon: require('../assets/images/support-category-icons/add.png')
          .default,
      },
      {
        title: 'Send',
        icon: require('../assets/images/support-category-icons/send.png')
          .default,
      },
      {
        title: 'Trade',
        icon: require('../assets/images/support-category-icons/trade.png')
          .default,
      },
      {
        title: 'Invest',
        icon: require('../assets/images/support-category-icons/invest.png')
          .default,
      },
    ],
  },
  {
    title: 'Actions',
    subMenu: [
      {
        title: 'Add',
        icon: require('../assets/images/support-category-icons/add.png')
          .default,
      },
      {
        title: 'Send',
        icon: require('../assets/images/support-category-icons/send.png')
          .default,
      },
      {
        title: 'Fiat Funding',
        icon: require('../assets/images/support-category-icons/trade.png')
          .default,
      },
      {
        title: 'Invest',
        icon: require('../assets/images/support-category-icons/invest.png')
          .default,
      },
    ],
  },
  {
    title: 'Learn',
    header: 'What Do You Want To Learn About?',
    subMenu: [],
  },
];

export const USERPOOL_ID = 'us-east-2_ALARK1RAa';

export const AGENCY_API_URL = 'https://fxagency.apimachine.com';

export const TELLERS_API_URL = 'https://teller2.apimachine.com';

export const MAILSURP_KEY =
  '86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c';

export const S3_CONFIG = {
  accessKeyId: Buffer.from('QUtJQVFENFFZWFdGV1dTVEdFNjQ=', 'base64').toString(),
  secretAccessKey: Buffer.from(
    'WElmckJsTG42OG1Dc2MxV3pMaVZrTnU2dkRuR1hSOW5UM2xCZExHZw==',
    'base64',
  ).toString(),
};

export const NEW_CHAT_API = 'https://testchatsioapi.globalxchange.io';
export const NEW_CHAT_SOCKET = 'https://testsockchatsio.globalxchange.io';

export const BRAIN_SECRET = 'uyrw7826^&(896GYUFWE&*#GBjkbuaf';
export const EMAIL_DEV = 'muhammad@nvestbank.com';
export const BRAIN_API_ENDPOINT = 'https://drivetest.globalxchange.io';
export const BRAIN_PATH = 'root/EngagementApp/';
