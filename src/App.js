import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from './Routes';
import { toast } from 'react-toastify';

const App = () => {
  useEffect(() => {
    toast.configure({ autoClose: 3000 });
    return () => {};
  }, []);

  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
};

export default App;
