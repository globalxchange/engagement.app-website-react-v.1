import axios from 'axios';
import { useEffect, useState } from 'react';
import { GX_API_ENDPOINT } from '../../configs';

const CompPlans = ({ item, onEdit, showEdit }) => {
  const [productDetails, setProductDetails] = useState('');
  const [appDetails, setAppDetails] = useState('');

  useEffect(() => {
    if (item) {
      axios
        .get(`${GX_API_ENDPOINT}/gxb/product/get`, {
          params: { product_id: item.product_id },
        })
        .then(({ data }) => {
          // console.log('Resp On ', data);

          const product = data.products ? data.products[0] : '';
          setProductDetails(product || '');
        })
        .catch((error) => {
          console.log('Error On', error);
        });

      axios
        .get(`${GX_API_ENDPOINT}/gxb/apps/get`, {
          params: { app_code: item.app_code },
        })
        .then(({ data }) => {
          // console.log('Resp On ', data);

          const app = data.apps ? data.apps[0] : '';
          setAppDetails(app || '');
        })
        .catch((error) => {
          console.log('Error On', error);
        });
    }
  }, [item]);

  return (
    <div className="comptroller-plan-item">
      <img
        src={productDetails.product_icon}
        alt=""
        className="comptroller-img"
      />
      <div className="product-details">
        <div className="item-name text-capitalize">
          {productDetails.product_name}
        </div>
        <div className="item-id text-capitalize">{item.product_id}</div>
      </div>
      <div className="app-details">
        <img src={appDetails?.app_icon} alt="" className="app-icon" />
        <div className="app-name">{appDetails?.app_name}</div>
      </div>
      <div className="chain-name">{item.comp_plan_id}</div>
      {showEdit && (
        <div className="edit-button" onClick={onEdit}>
          <img
            src={require('../../assets/images/edit-dark-icon.svg').default}
            alt=""
            className="edit-icon"
          />
        </div>
      )}
    </div>
  );
};

export default CompPlans;
