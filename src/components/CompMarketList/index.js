/* eslint-disable no-restricted-syntax */
import axios from 'axios';
import { Fragment, useEffect, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import { GX_API_ENDPOINT } from '../../configs';
import { usdValueFormatterWithoutSign } from '../../utils';
import CompMarketEditModal from '../CompMarketEditModal';
import CompPlans from './CompPlans';

const CompMarketList = ({ activeCategory, searchInput, paramsValue }) => {
  const [compChains, setCompChains] = useState();
  const [compPlans, setCompPlans] = useState();
  const [compLaws, setCompLaws] = useState();
  const [compBlocks, setCompBlocks] = useState();
  const [compItems, setCompItems] = useState();
  const [filterList, setFilterList] = useState();
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editObject, setEditObject] = useState('');
  const [isReload, setIsReload] = useState(false);

  useEffect(() => {
    const parseParams = {};

    for (const [key, value] of Object.entries(paramsValue)) {
      if (value !== 'all') {
        parseParams[key] = value;
      }
    }

    // console.log('parseParams', parseParams);

    let compChainsParam = {};
    let compPlansParam = {};
    let compLawsParam = {};
    let compBlocksParam = {};
    let compItemsParam = {};

    switch (activeCategory?.title) {
      case 'CompChains':
        compChainsParam = {
          ...parseParams,
          created_by: parseParams.email,
          email: undefined,
        };
        setCompChains(null);
        break;
      case 'CompPlans':
        compPlansParam = parseParams;
        setCompPlans(null);
        break;
      case 'CompLaws':
        compLawsParam = parseParams;
        setCompLaws(null);
        break;
      case 'CompBlocks':
        compBlocksParam = parseParams;
        setCompBlocks(null);
        break;
      case 'CompItems':
        compItemsParam = parseParams;
        setCompItems(null);
        break;
      default:
    }
    axios
      .get(`${GX_API_ENDPOINT}/gxb/apps/broker/comp/plan/get`, {
        params: compChainsParam,
      })
      .then(({ data }) => {
        const items = data?.comp_plans || [];

        setCompChains(items);
      })
      .catch((error) => {});

    axios
      .get(`${GX_API_ENDPOINT}/gxb/product/commission/fees/get`, {
        params: compPlansParam,
      })
      .then(({ data }) => {
        const items = data?.commissionData || [];

        setCompPlans(items);
      })
      .catch((error) => {});

    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/params`, {
        params: compLawsParam,
      })
      .then(({ data }) => {
        const items = data?.params || [];

        setCompLaws(items);
      })
      .catch((error) => {});

    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/blocks`, {
        params: compBlocksParam,
      })
      .then(({ data }) => {
        const items = data?.comp_blocks || [];

        setCompBlocks(items);
      })
      .catch((error) => {});

    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/fields`, {
        params: compItemsParam,
      })
      .then(({ data }) => {
        const items = data?.fields || [];

        setCompItems(items);
      })
      .catch((error) => {});
  }, [paramsValue, activeCategory, isReload]);

  useEffect(() => {
    const searchQuery = searchInput.trim().toLowerCase();

    let list = null;

    switch (activeCategory?.title) {
      case 'CompChains':
        list = compChains?.filter(
          (x) =>
            x.name?.toLowerCase().includes(searchQuery) ||
            x.created_by?.toLowerCase().includes(searchQuery),
        );
        break;
      case 'CompPlans':
        list = compPlans?.filter(
          (x) =>
            x.product_id?.toLowerCase().includes(searchQuery) ||
            x.comp_plan_id?.toLowerCase().includes(searchQuery) ||
            x.app_code?.toLowerCase().includes(searchQuery),
        );
        break;
      case 'CompLaws':
        list = compLaws?.filter((x) =>
          x.applied_to?.toLowerCase().includes(searchQuery),
        );
        break;
      case 'CompBlocks':
        list = compBlocks?.filter(
          (x) =>
            x.comp_block_id?.toLowerCase().includes(searchQuery) ||
            x.level?.toString().toLowerCase().includes(searchQuery),
        );
        break;
      case 'CompItems':
        list = compItems?.filter(
          (x) =>
            x.name?.toLowerCase().includes(searchQuery) ||
            x.collection?.toLowerCase().includes(searchQuery),
        );
        break;
      default:
    }
    setFilterList(list);
  }, [
    searchInput,
    activeCategory,
    compChains,
    compPlans,
    compLaws,
    compBlocks,
    compItems,
  ]);

  const renderList = () => {
    if (!filterList) {
      return Array(20)
        .fill('')
        .map((_, index) => (
          <Fragment key={index}>
            {activeCategory?.title === 'CompPlans' ? (
              <div className="comptroller-plan-item">
                <Skeleton className="comptroller-img" height={140} />
                <div className="product-details">
                  <Skeleton className="item-name" height={20} />
                  <Skeleton className="item-id" height={10} />
                </div>
                <div className="app-details">
                  <Skeleton width={20} height={20} />
                  <Skeleton className="app-name ml-1" height={20} width={100} />
                </div>
                <div className="chain-name">
                  <Skeleton className="chain-name" height={10} width={200} />
                </div>
              </div>
            ) : (
              <div className="comptroller-item">
                <Skeleton className="comptroller-img" height={140} />
                <Skeleton className="item-name" height={20} />
                <Skeleton className="item-created-by" height={10} />
              </div>
            )}
          </Fragment>
        ));
    }

    if (filterList?.length <= 0) {
      return <div className="empty-text">No {activeCategory?.title} Found</div>;
    }

    switch (activeCategory?.title) {
      case 'CompChains':
        return filterList?.map((item, index) => (
          <div
            key={index}
            className={`comptroller-item ${
              paramsValue?.email ? 'show-edit' : ''
            }`}
          >
            <img src={item.icon} alt="" className="comptroller-img" />
            <div className="item-name text-capitalize">{item.name}</div>
            <div className="item-created-by">{item.created_by}</div>
            <div
              className="edit-button"
              onClick={() => {
                setEditObject(item);
                setIsEditModalOpen(true);
              }}
            >
              <img
                src={require('../../assets/images/edit-dark-icon.svg').default}
                alt=""
                className="edit-icon"
              />
            </div>
          </div>
        ));

      case 'CompPlans':
        return filterList?.map((item) => (
          <CompPlans
            key={item._id}
            item={item}
            onEdit={() => {
              setEditObject(item);
              setIsEditModalOpen(true);
            }}
            showEdit={!!paramsValue?.email}
          />
        ));

      case 'CompLaws':
        return filterList?.map((item, index) => (
          <div
            key={index}
            className={`comptroller-item ${
              paramsValue?.email ? 'show-edit' : ''
            }`}
          >
            <img src={item.icon} alt="" className="comptroller-img" />
            <div className="item-name text-capitalize">
              {JSON.stringify(item.value)}
            </div>
            <div className="item-created-by text-capitalize">
              Applied To {item.applied_to}
            </div>
            <div
              className="edit-button"
              onClick={() => {
                setEditObject(item);
                setIsEditModalOpen(true);
              }}
            >
              <img
                src={require('../../assets/images/edit-dark-icon.svg').default}
                alt=""
                className="edit-icon"
              />
            </div>
          </div>
        ));

      case 'CompBlocks':
        return filterList?.map((item, index) => (
          <div
            key={index}
            className={`comptroller-item ${
              paramsValue?.email ? 'show-edit' : ''
            }`}
          >
            <div className="item-value">
              {usdValueFormatterWithoutSign.format(item.percentage || 0)}%
            </div>
            <div className="item-name">{item.comp_block_id}</div>
            <div className="item-created-by">Level {item.level}</div>
            <div
              className="edit-button"
              onClick={() => {
                setEditObject(item);
                setIsEditModalOpen(true);
              }}
            >
              <img
                src={require('../../assets/images/edit-dark-icon.svg').default}
                alt=""
                className="edit-icon"
              />
            </div>
          </div>
        ));

      case 'CompItems':
        return filterList?.map((item, index) => (
          <div
            key={index}
            className={`comptroller-item ${
              paramsValue?.email ? 'show-edit' : ''
            }`}
          >
            <img src={item.icon} alt="" className="comptroller-img" />
            <div className="item-name">{item.name}</div>
            <div className="item-created-by">{item.collection}</div>
            <div
              className="edit-button"
              onClick={() => {
                setEditObject(item);
                setIsEditModalOpen(true);
              }}
            >
              <img
                src={require('../../assets/images/edit-dark-icon.svg').default}
                alt=""
                className="edit-icon"
              />
            </div>
          </div>
        ));
      default:
    }
  };

  return (
    <div className="comptroller-list-container">
      {renderList()}
      <CompMarketEditModal
        activeCategory={activeCategory}
        isOpen={isEditModalOpen}
        onClose={() => setIsEditModalOpen(false)}
        editObject={editObject}
        reloadData={() => setIsReload(!isReload)}
      />
    </div>
  );
};

export default CompMarketList;
