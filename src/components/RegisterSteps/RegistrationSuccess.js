import React from 'react';
import { Link } from 'react-router-dom';

const RegistrationSuccess = () => {
  return (
    <div className="register-component">
      <h6 className="sub-header">Congrats. You Have Completed Registration</h6>
      <Link to="/login" className="next-btn">
        Login
      </Link>
    </div>
  );
};

export default RegistrationSuccess;
