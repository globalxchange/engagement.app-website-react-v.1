import React, { useState, useContext, useEffect } from 'react';
import { toast } from 'react-toastify';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';
import { emailValidator } from '../utils';
import {
  APP_CODE,
  GX_API_ENDPOINT,
  GX_AUTH_URL,
  NEW_CHAT_API,
} from '../configs';
import LocalStorageHelper from '../utils/LocalStorageHelper';
import { AppContext } from '../contexts/AppContext';
import ButtonLoadingAnimation from './ButtonLoadingAnimation';
import SideImage from '../assets/images/login-side-image.jpg';
import AppLogo from '../assets/images/engagement-app-logo.svg';
import LoginLoadingAnimation from './LoginLoadingAnimation';
import LoginBrandSelector from './LoginBrandSelector';
import OtpInput from 'react-otp-input';
import PasswordResetFrom from './PasswordResetForm';

const LoginFrom = ({ appCode }) => {
  const { setRefreshData, setActiveOperatorApp, setActiveBrand } =
    useContext(AppContext);

  const history = useHistory();

  const [emailInput, setEmailInput] = useState('');
  const [passwordInput, setPasswordInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [loginData, setLoginData] = useState('');
  const [show2FA, setShow2FA] = useState(false);
  const [otpInput, setOtpInput] = useState('');
  const [brandsData, setBrandsData] = useState();
  const [isPasswordRestOpen, setIsPasswordRestOpen] = useState(false);

  useEffect(() => {
    if (otpInput.trim().length === 6) {
      onFormSubmit();
    }
  }, [otpInput]);

  const registerUserInChat = (userResp, token) => {
    Axios.post(
      `${NEW_CHAT_API}/get_application`,
      { code: APP_CODE },
      { headers: { email: userResp?.user?.email, token } },
    )
      .then(({ data }) => {
        // console.log('Data', data);

        const appId = data?.payload?.id || '';

        // console.log('userResp', userResp);
        const registerData = {
          first_name: userResp?.user?.first_name || userResp?.user?.name,
          last_name: userResp?.user?.last_name || ' ',
          username: userResp?.user?.username,
          bio: userResp?.user?.bio || 'None',
          email: userResp?.user?.email || '',
          timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
          avatar: userResp?.user?.profile_img || ' ',
        };
        // console.log('registerData', registerData);
        Axios.post(`${NEW_CHAT_API}/register_with_chatsio`, registerData)
          .then(({ data: chatData }) => {
            // console.log('Data', chatData);
            const postData = {
              email: userResp?.user?.email || '',
              app_id: appId,
            };
            Axios.post(`${NEW_CHAT_API}/register_user`, postData);
          })
          .catch((error) => {
            console.log('Error on registering user', error);
          });
      })
      .catch((error) => {
        console.log('Error on getting application', error);
      });
  };

  const getUserDetails = (email, token) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: { email },
    })
      .then((res) => {
        const { data } = res;

        // console.log('Data', data);
        if (data.status) {
          registerUserInChat(data, token);
        }
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  const getBrands = (email) => {
    setIsLoading(true);

    Axios.get(`${GX_API_ENDPOINT}/gxb/app/gxlive/user/operator/get`, {
      params: { email, show_apps: true },
    })
      .then(({ data }) => {
        // console.log('Data', data);
        setBrandsData(data?.operators || []);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log('Error on getting brand', error);
      });
  };

  const onFormSubmit = (event) => {
    if (event?.preventDefault) {
      event.preventDefault();
    }

    if (!isLoading) {
      const email = emailInput.toLowerCase().trim();

      if (!email) {
        toast.error('📧 Please enter your email');
      } else if (!emailValidator(email)) {
        toast.error('📧 Please enter a valid email');
      } else if (!passwordInput) {
        toast.error('🔑 Please enter your password');
      } else {
        setIsLoading(true);

        let postData = {
          email,
          password: passwordInput,
        };

        if (show2FA) {
          postData = { ...postData, totp_code: otpInput };
        }

        Axios.post('https://gxauth.apimachine.com/gx/user/login', postData)
          .then((response) => {
            const { data } = response;

            // console.log('Login Data', data);
            if (data.mfa) {
              setShow2FA(true);
              setIsLoading(false);
            } else if (data.status) {
              // toast.success('✅ Logged in...');

              setLoginData(data);
              getUserDetails(email, data.idToken);

              Axios.post(`${GX_API_ENDPOINT}/gxb/apps/register/user`, {
                email,
                app_code: appCode || APP_CODE,
              }).then((profileResp) => {
                // console.log('profileResp', profileResp.data);

                if (profileResp.data.profile_id) {
                  LocalStorageHelper.setProfileId(profileResp.data.profile_id);
                  setRefreshData();
                }
              });

              Axios.post(`${GX_API_ENDPOINT}/get_affiliate_data`, {
                email,
              })
                .then((res) => {
                  LocalStorageHelper.setUserDetails(
                    res.data.name,
                    `0x${res.data.ETH_Address}`,
                    res.data.affiliate_id,
                  );
                })
                .catch((error) => {
                  console.log('getUserDetails Error', error);
                })
                .finally(() => {
                  // history.push('/dashboard');
                  getBrands(email);
                });
            } else {
              setIsLoading(false);
              // if (show2FA) {
              //   setOtpInput('');
              // } else {
              //   setPasswordInput('');
              // }
              toast.error(`❌ ${data.message}`);
            }
          })
          .catch((error) => {
            console.log('Login Error', error);
          });
      }
    } else {
      toast.warn('Logging in...');
    }
  };

  const navigateToDashboard = (selectedBrand, app) => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
      setActiveBrand(selectedBrand);
      if (app) {
        setActiveOperatorApp(app);
      }
      LocalStorageHelper.setAppLoginData(
        loginData.idToken,
        loginData.accessToken,
        loginData.user?.email,
        selectedBrand,
      );

      history.push('/dashboard');
    }, 1000);
  };

  const onRestPasswordRequest = () => {
    const email = emailInput.trim().toLowerCase();

    if (!email) {
      return toast.error('Please Input Your Email Address First');
    }

    Axios.post(`${GX_AUTH_URL}/gx/user/password/forgot/request`, {
      email,
    });

    setIsPasswordRestOpen(true);
  };

  return (
    <>
      {brandsData ? (
        <LoginBrandSelector
          brandsData={brandsData}
          navigateToDashboard={navigateToDashboard}
        />
      ) : (
        <div className="login-page-wrapper container-fluid">
          <div className="row flex-fill">
            <div className="col-md-5 px-5">
              <form className="login-form px-5" onSubmit={onFormSubmit}>
                <img className="broker-logo" src={AppLogo} alt="" />
                <div className="login-action-container">
                  {isPasswordRestOpen ? (
                    <PasswordResetFrom
                      email={emailInput}
                      setIsLoading={setIsLoading}
                      onClose={() => setIsPasswordRestOpen(false)}
                    />
                  ) : show2FA ? (
                    <>
                      <div className="otp-header">
                        Enter The 2FA Code From Google Authenticator
                      </div>
                      <OtpInput
                        value={otpInput}
                        onChange={(otp) => setOtpInput(otp)}
                        numInputs={6}
                        containerStyle="otp-container"
                        inputStyle="input-field otp-input"
                        shouldAutoFocus
                      />
                    </>
                  ) : (
                    <>
                      <input
                        type="text"
                        className="form-input"
                        placeholder="Email"
                        value={emailInput}
                        onChange={(e) => setEmailInput(e.target.value)}
                      />
                      <input
                        type="password"
                        className="form-input"
                        placeholder="Password"
                        value={passwordInput}
                        onChange={(e) => setPasswordInput(e.target.value)}
                      />
                      <button className="actions-btn inverted" type="submit">
                        {isLoading ? 'Loggin In' : 'Login'}{' '}
                        {isLoading && <ButtonLoadingAnimation />}
                      </button>
                      <div
                        className="passwd-reset-text"
                        onClick={onRestPasswordRequest}
                      >
                        <span>Click Here</span> To Reset Password
                      </div>
                    </>
                  )}
                </div>
              </form>
            </div>
            <div className="col-md-7 px-0 d-flex flex-column">
              <img src={SideImage} alt="" className="side-image" />
            </div>
          </div>
        </div>
      )}
      {isLoading && (
        <div className="login-loader-container">
          <LoginLoadingAnimation />
          <div className="loading-text">Loading Up Your Enagement App</div>
        </div>
      )}
    </>
  );
};

export default LoginFrom;
