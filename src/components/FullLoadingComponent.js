import React from 'react';

const FullLoadingComponent = () => (
  <div className="full-loading-wrapper">
    <img
      src={require('../assets/images/engagement-app-logo.svg').default}
      alt=""
      className="full-loading-logo"
    />
  </div>
);

export default FullLoadingComponent;
