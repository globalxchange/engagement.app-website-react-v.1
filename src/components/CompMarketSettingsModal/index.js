import { useEffect, useState } from 'react';
import PopupModalLayout from '../../layouts/PopupModalLayout';
import Switch from 'react-switch';
import LocalStorageHelper from '../../utils/LocalStorageHelper';

const CompMarketSettingsModal = ({
  isOpen,
  onClose,
  settings = [],
  setParamsValue,
  paramsValue,
  activeCategory,
}) => {
  const [openedList, setOpenedList] = useState('');
  const [isShowSave, setIsShowSave] = useState(false);
  const [tempParamValue, setTempParamValue] = useState({});
  const [searchText, setSearchText] = useState('');
  const [filterList, setFilterList] = useState([]);

  useEffect(() => {
    if (!isOpen) {
      setFilterList([]);
      setSearchText('');
      setTempParamValue({});
      setIsShowSave(false);
      setOpenedList('');
    }
  }, [isOpen]);

  useEffect(() => {
    if (openedList?.list) {
      const searchQuery = searchText.trim().toLowerCase();

      const filter = openedList?.list.filter((x) =>
        x.name.toLowerCase().includes(searchQuery),
      );
      setFilterList(filter);
    } else {
      setFilterList([]);
    }
  }, [openedList, searchText]);

  const onItemClick = (item) => {
    const temp = paramsValue ? { ...paramsValue } : {};
    temp[`${openedList.queryParam}`] = item.paramValue;
    setTempParamValue(temp);

    setOpenedList('');
    setIsShowSave(true);
  };

  const onSaveClick = () => {
    setParamsValue(tempParamValue);
    onClose();
  };

  const onSwitchChange = (checked) => {
    const temp = paramsValue ? { ...paramsValue } : {};
    temp.email = checked ? undefined : LocalStorageHelper.getAppEmail();
    setParamsValue(temp);
  };

  const getActiveItem = (optionItem) => {
    const param =
      tempParamValue[optionItem.queryParam] ||
      paramsValue[optionItem.queryParam];

    const item = optionItem?.list?.find((x) => x.paramValue === param);

    return item?.name || optionItem.subTitle;
  };

  return (
    <PopupModalLayout
      isOpen={isOpen}
      onClose={onClose}
      noHeader
      style={{ padding: 0 }}
      width={700}
    >
      <div className="comp-market-settings">
        <div className="header-container">
          <img src={activeCategory?.icon} alt="" className="header-image" />
          <div className="header-name">{activeCategory?.title}</div>
          <div className="switch-container">
            <span>Only Mine</span>
            <Switch
              onChange={onSwitchChange}
              checked={!paramsValue.email}
              uncheckedIcon={false}
              checkedIcon={false}
              className="react-switch"
              id="material-switch"
            />
            <span>All</span>
          </div>
        </div>
        <div className="settings-container">
          <div className="settings-title">Page</div>
          <div className="bread-crumbs-container">
            <div className="bread-crumb-item" onClick={() => setOpenedList('')}>
              <div className="item-name">{activeCategory?.title}</div>
              <span className="arrow">{'->'}</span>
            </div>
            {openedList && (
              <div className="bread-crumb-item">
                <div className="item-name">{openedList.title}</div>
                <span className="arrow">{'->'}</span>
              </div>
            )}
          </div>
          {openedList && (
            <div className="search-container">
              <input
                type="text"
                className="search-input"
                value={searchText}
                onChange={(e) => setSearchText(e.target.value)}
                placeholder={`Search All ${openedList.title}...`}
              />
            </div>
          )}
          <div className="list-container">
            {openedList
              ? filterList?.map((item) => (
                  <div
                    key={item._id || item.name}
                    className={`list-item ${
                      item.paramValue ===
                      (isShowSave
                        ? tempParamValue[`${openedList.queryParam}`]
                        : paramsValue[`${openedList.queryParam}`])
                        ? 'active'
                        : ''
                    }`}
                    onClick={() => onItemClick(item)}
                  >
                    <img src={item.icon} alt="" className="item-icon" />
                    <div className="item-name">{item.name}</div>
                  </div>
                ))
              : settings.map((item) => (
                  <div
                    key={item.title}
                    className="option-item"
                    onClick={() => setOpenedList(item)}
                  >
                    <div className="d-flex flex-column">
                      <div className="option-title">{item.title}</div>
                      <div className="option-sub-title">
                        {getActiveItem(item)}
                      </div>
                    </div>
                    <svg
                      width="11"
                      height="16"
                      viewBox="0 0 11 16"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M10.8231 7.54637L4.05595 0.212949C3.93074 0.077436 3.75514 0 3.57121 0H0.663291C0.0850321 0 -0.215268 0.69431 0.178551 1.12073L6.53161 8L0.178551 14.8793C-0.215268 15.3057 0.0850321 16 0.663291 16H3.57121C3.75514 16 3.93074 15.9231 4.05595 15.787L10.8231 8.45363C11.059 8.19778 11.059 7.80222 10.8231 7.54637Z"
                        fill="#383C41"
                      />
                    </svg>
                  </div>
                ))}
          </div>
        </div>
        <div className="action-container">
          <div className="action-item">Search</div>
          <div
            className="action-item primary"
            onClick={isShowSave ? onSaveClick : null}
          >
            {isShowSave ? 'Save' : 'Page'}
          </div>
        </div>
      </div>
    </PopupModalLayout>
  );
};

export default CompMarketSettingsModal;
