import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../contexts/AppContext';

const SiteHeader = ({ openMenu }) => {
  const { isMobile } = useContext(AppContext);

  return (
    <div className="site-header">
      <img
        src={require('../assets/images/hamburger-dark.svg').default}
        alt=""
        className="menu-icon"
        onClick={openMenu}
      />
      <div className="app-logo-container">
        <Link to="/">
          <img
            src={require('../assets/images/engagement-app-logo.svg').default}
            alt=""
            className="broker-logo"
          />
        </Link>
      </div>
      <a
        href="https://app.influencecoin.com/"
        target="_blank"
        rel="noopener noreferrer"
        className="header-action-button"
      >
        <img
          src={require('../assets/images/influence-app-logo.svg').default}
          alt=""
          className="influence-icon"
        />
      </a>
    </div>
  );
};

export default SiteHeader;
