import React, { useState } from 'react';
import AppLinkViewer from './AppLinkViewer';

const InstallerType = ({ selectedPlatform }) => {
  const [installerType, setInstallerType] = useState();

  if (installerType) {
    return <AppLinkViewer selectedPlatform={selectedPlatform} />;
  }

  return (
    <div className="installer-container">
      <div className="title">Next Step</div>
      <div
        onClick={() => setInstallerType('Download')}
        className="selection-item"
      >
        <div className="item-label">Download App On This Device</div>
      </div>
      <div className="selection-item disabled">
        <div className="item-label">Send Me The Download Link</div>
      </div>
    </div>
  );
};

export default InstallerType;
