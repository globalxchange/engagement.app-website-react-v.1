import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import InstallerType from './InstallerType';

const AppInstaller = () => {
  const { platform } = useParams();

  const [selectedPlatform, setSelectedPlatform] = useState();

  useEffect(() => {
    if (platform === 'android') {
      setSelectedPlatform('Android');
    } else if (platform === 'ios') {
      setSelectedPlatform('iOS');
    }
  }, [platform]);

  if (selectedPlatform) {
    return <InstallerType selectedPlatform={selectedPlatform} />;
  }

  return (
    <div className="installer-container">
      <div className="title">Select Device</div>
      <div
        onClick={() => setSelectedPlatform('Android')}
        className="selection-item"
      >
        <img
          src={require('../../assets/images/android-icon.svg').default}
          alt=""
          className="item-icon"
        />
        <div className="item-name">Android</div>
      </div>
      <div
        onClick={() => setSelectedPlatform('iOS')}
        className="selection-item"
      >
        <img
          src={require('../../assets/images/ios-icon.svg').default}
          alt=""
          className="item-icon"
        />
        <div className="item-name">iPhone</div>
      </div>
    </div>
  );
};

export default AppInstaller;
