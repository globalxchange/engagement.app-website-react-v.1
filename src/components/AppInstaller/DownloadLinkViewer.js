import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const DownloadLinkViewer = ({ link }) => {
  const [isCopied, setIsCopied] = useState(false);

  const onCopyHandler = () => {
    navigator.clipboard.writeText(link);
    setIsCopied(true);

    setTimeout(() => setIsCopied(false), 8000);
  };

  return (
    <div className="installer-container">
      <div className="title">Download Link</div>
      <div onClick={onCopyHandler} className="selection-item">
        <div className="item-link">
          {isCopied ? 'Copied To Your Clipboard' : link}
        </div>
        <img
          src={require('../../assets/images/copy-icon-colored.svg').default}
          alt=""
          className="copy-icon"
        />
      </div>
      <Link to="/mobile-landing" className="home-button">
        Back Home
      </Link>
    </div>
  );
};

export default DownloadLinkViewer;
