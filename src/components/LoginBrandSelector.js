import React, { useEffect, useState } from 'react';
import appLogo from '../assets/images/engagement-logo-white.svg';
import { faPlus, faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import editIcon from '../assets/images/edit-dark-icon.svg';
import arrowIcon from '../assets/images/dotted-arrow-white.svg';
import Skeleton from 'react-loading-skeleton';

const LoginBrandSelector = ({ brandsData, navigateToDashboard }) => {
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [filteredList, setFilteredList] = useState();
  const [isAppsSelected, setIsAppsSelected] = useState(false);
  const [appsList, setAppsList] = useState();

  useEffect(() => {
    if (!isSearchOpen) {
      setSearchText('');
    }
  }, [isSearchOpen]);

  useEffect(() => {
    if (brandsData) {
      const apps = [];

      brandsData.map(item => {
        if (item.app_data) {
          apps.push(...item.app_data);
        }
      });

      // console.log('apps', apps);
      setAppsList(apps);
    }
  }, [brandsData]);

  useEffect(() => {
    if (brandsData) {
      const searchQuery = searchText.toLowerCase().trim();

      let list = [];

      if (isAppsSelected) {
        list = appsList.filter(
          x =>
            x.app_name?.toLowerCase().includes(searchQuery) ||
            x.app_code?.toLowerCase().includes(searchQuery),
        );
      } else {
        list = brandsData.filter(
          x =>
            x.brand_name?.toLowerCase().includes(searchQuery) ||
            x.brand_email?.toLowerCase().includes(searchQuery),
        );
      }

      setFilteredList(list);
    }
  }, [brandsData, searchText, appsList, isAppsSelected]);

  const onBrandClick = item => {
    let brandData;
    let appData;

    if (isAppsSelected) {
      appData = item;

      brandData = brandsData.find(x => x.app_data.includes(item));
    } else {
      brandData = item;
      appData = null;
    }

    // console.log('brandData', brandData);
    // console.log('appData', appData);

    navigateToDashboard(brandData, appData);
  };

  return (
    <div className="login-brand-selector-wrapper">
      <div className="brand-selector-container">
        <div className="header-container">
          <div
            className="header-action"
            onClick={() =>
              brandsData?.length > 0 ? setIsSearchOpen(!isSearchOpen) : null
            }
          >
            <FontAwesomeIcon
              icon={isSearchOpen ? faTimes : faSearch}
              className="header-action-icon"
            />
          </div>
          <div className="logo-container">
            <img src={appLogo} alt="" className="app-logo" />
          </div>
          <div className="flex-fill" />
          {brandsData?.length > 0 || (
            <div className="arrow-container">
              <img src={arrowIcon} alt="" className="arrow-icon" />
              <img src={arrowIcon} alt="" className="arrow-icon" />
            </div>
          )}
          <div className="header-action">
            <FontAwesomeIcon icon={faPlus} className="header-action-icon" />
          </div>
        </div>
        <div className={`search-container ${isSearchOpen ? '' : 'h-0'}`}>
          <input
            type="text"
            placeholder={`Search For One Of Your ${
              isAppsSelected ? 'Apps' : 'Brands'
            }...`}
            value={searchText}
            onChange={e => setSearchText(e.target.value)}
          />
          <div
            onClick={() => setIsAppsSelected(true)}
            className={`list-switcher ${isAppsSelected ? 'active' : ''}`}
          >
            Apps
          </div>
          <div
            onClick={() => setIsAppsSelected(false)}
            className={`list-switcher ${isAppsSelected ? '' : 'active'}`}
          >
            Brands
          </div>
        </div>
        {brandsData?.length > 0 && !isSearchOpen && (
          <div className="list-header">Select A Brand To Continue</div>
        )}

        <div className="brand-list-container">
          {brandsData ? (
            brandsData?.length > 0 ? (
              filteredList?.length > 0 ? (
                filteredList?.map(item => (
                  <div
                    key={item._id}
                    className="brand-item"
                    onClick={() => onBrandClick(item)}
                  >
                    <div className="bran-img-container">
                      <img
                        src={item?.brand_logo || item?.app_icon}
                        alt=""
                        className="brand-image"
                      />
                    </div>
                    <div className="brand-name">
                      {item?.brand_name || item?.app_name}
                    </div>
                    {isAppsSelected ? null : (
                      <div className="brand-app-no">
                        {item?.app_data?.length || 0} Apps
                      </div>
                    )}
                    <div className="edit-button">
                      <img alt="" src={editIcon} className="edit-icon" />
                    </div>
                  </div>
                ))
              ) : (
                <div className="empty-text">
                  No {isAppsSelected ? 'Apps' : 'Brands'} Found...
                </div>
              )
            ) : (
              <div className="empty-text">
                People Can’t Enage With Your Brands Yet Cause They Don’t Exist.
                Don’t Worry Click Here And We Will Help You Launch Your Brand In
                Minutes
              </div>
            )
          ) : (
            Array(4)
              .fill(1)
              .map((_, index) => (
                <div key={index} className="brand-item">
                  <Skeleton width={100} height={100} circle />
                  <Skeleton width={100} height={10} className="brand-name" />
                </div>
              ))
          )}
        </div>
      </div>
    </div>
  );
};

export default LoginBrandSelector;
