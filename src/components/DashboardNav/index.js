import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import brokerAppLogo from '../../assets/images/gx-broker-app-icon-white.png';
import { AppContext } from '../../contexts/AppContext';
import { nameFormatter, usdValueFormatter } from '../../utils';
import LocalStorageHelper from '../../utils/LocalStorageHelper';
import NavItems from './NavItems';
import arrowIcon from '../../assets/images/chevron-left.svg';

const DashboardNav = () => {
  const {
    walletCoinData,
    isNavCollapsed,
    setIsNavCollapsed,
    setIsBrandSwicherOpen,
  } = useContext(AppContext);

  let totalBalanceInUsd = 0;

  if (walletCoinData) {
    walletCoinData.forEach(item => {
      totalBalanceInUsd += item?.coinValueUSD || 0;
    });
  }

  const brandData = LocalStorageHelper.getLoggedBrandData();

  return (
    <div
      className={`broker-app-nav-wrapper ${isNavCollapsed ? 'closed' : 'open'}`}
    >
      <div className="brand-wrapper">
        <img
          src={brandData?.brand_logo || brokerAppLogo}
          alt=""
          className="brand-img"
          onClick={() => setIsBrandSwicherOpen(true)}
        />
        <div
          className="expand-toggle"
          onClick={() => setIsNavCollapsed(!isNavCollapsed)}
        >
          <img
            src={arrowIcon}
            alt=""
            className="arrow-icon"
            style={{
              transform: `rotate(${isNavCollapsed ? '180deg' : '0deg'})`,
            }}
          />
        </div>
      </div>
      <div className="broker-app-nav">
        <div className="broker-details">
          {brandData ? (
            <div className="broker-name">
              {nameFormatter(brandData?.brand_name)}
            </div>
          ) : null}
          <div className="broker-valuation">{brandData?.brand_email}</div>
        </div>
        <NavItems />
      </div>
    </div>
  );
};

export default DashboardNav;
