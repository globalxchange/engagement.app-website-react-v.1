import React, { useEffect } from 'react';

const DashboardTopMenu = ({ activeNavMenu, setActiveNavMenu }) => {
  useEffect(() => {
    setActiveNavMenu(MENU_ITEMS[0]);
  }, []);

  return (
    <div className="main-nav-wrapper profile-top-nav">
      {MENU_ITEMS.map(item => (
        <div
          key={item.title}
          className={`menu-item ${
            activeNavMenu?.title === item.title ? 'active' : ''
          }`}
          onClick={() => setActiveNavMenu(item)}
        >
          <div className="menu-title">{item.title}</div>
        </div>
      ))}
      <div className="add-button">
        <img
          src={require('../assets/images/close-close.svg').default}
          alt=""
          className="add-icon"
        />
      </div>
    </div>
  );
};

export default DashboardTopMenu;

const MENU_ITEMS = [
  { title: 'Internal' },
  { title: 'Opportunities', disabled: true },
  { title: 'Facebook', disabled: true },
  { title: 'Google', disabled: true },
  { title: 'Instagram', disabled: true },
  { title: 'LinkedIn', disabled: true },
  { title: 'YouTube', disabled: true },
];
