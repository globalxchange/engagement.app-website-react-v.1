/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { formatterHelper, usdValueFormatter } from '../../../../../../utils';

const AssetItem = ({
  img,
  name,
  price,
  symbol,
  setCoinObject,
  coinObject,
  transCoin,
  setTransCoin,
}) => {
  return (
    <div
      className={`asset-item d-flex p-4 justify-content-between ${
        symbol === transCoin ? 'active' : ''
      }`}
      tabIndex="0"
      role="button"
      onClick={() => {
        setTransCoin(symbol);
        setCoinObject(coinObject);
      }}
    >
      <div className="d-flex col-5 px-2">
        <img src={img} className="icon my-auto" alt="" />
        <h4 className="mx-2">{name}</h4>
      </div>
      <h4 className="col p-0">
        {formatterHelper(coinObject?.coinValue || 0, symbol)}
      </h4>
      <h4 className="px-2">${usdValueFormatter.format(price)}</h4>
    </div>
  );
};

export default AssetItem;
