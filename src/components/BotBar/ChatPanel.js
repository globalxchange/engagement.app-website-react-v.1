import React, { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import chatsIcon from '../../assets/images/chats-icon-white.svg';
import gxBrainIcon from '../../assets/images/gx-brain-icon.png';
import gxPhoneIcon from '../../assets/images/gx-phone.png';
import gxPulse from '../../assets/images/gx-pulse-icon.png';
import gxVaultIcon from '../../assets/images/gx-vault-icon.png';
import searchIcon from '../../assets/images/search-icon.svg';

const suggestions = [
  { actionName: 'Buy', keyWords: ['bitcoin', 'ethereum', 'gxt', 'buy'] },
  { actionName: 'Sell', keyWords: ['bitcoin', 'ethereum', 'gxt', 'sell'] },
  {
    actionName: 'Convert',
    keyWords: ['bitcoin', 'ethereum', 'gxt', 'convert']
  },
  { actionName: 'Trade', keyWords: ['trade'] }
];

const ChatPanel = ({ close }) => {
  const [searchInput, setSearchInput] = useState('');
  const [suggestionList, setSuggestionList] = useState([]);

  useEffect(() => {
    if (searchInput && searchInput.length >= 2) {
      const suggestionArry = [];

      suggestions.forEach(item => {
        item.keyWords.forEach(keyWord => {
          if (
            keyWord
              .toLocaleLowerCase()
              .includes(searchInput.toLocaleLowerCase())
          ) {
            return suggestionArry.push(item.actionName);
          }
          return null;
        });
      });
      setSuggestionList(suggestionArry);
    } else {
      setSuggestionList([]);
    }
  }, [searchInput]);

  return (
    <div className="chat-input-area animated slideInUp faster">
      <div className="action-suggested-wrapper">
        <div className="action-list">
          {suggestionList.map(item => (
            <div
              key={item}
              className="suggested-action"
              onClick={() => toast.info(`Clicked on ${item}`)}
            >
              {item}
            </div>
          ))}
        </div>
      </div>

      <form
        className="chat-input-form"
        onSubmit={e => {
          e.preventDefault();
        }}
      >
        <input
          className="chat-input"
          type="text"
          placeholder="Start typing an action..."
          value={searchInput}
          onChange={e => setSearchInput(e.target.value)}
        />
        <button type="submit" className="chat-submit-btn">
          <img src={searchIcon} alt="" />
        </button>
      </form>
      <div className="chat-action-opened-wrapper">
        <div className="chat-action">
          <img src={gxBrainIcon} alt="" />
        </div>
        <div className="chat-action">
          <img src={gxPhoneIcon} alt="" />
        </div>
        <div className="chat-close-action-wrapper">
          <div className="chat-close-action" onClick={close}>
            <img src={chatsIcon} alt="" />
          </div>
        </div>
        <div className="chat-action">
          <img src={gxPulse} alt="" />
        </div>
        <div className="chat-action">
          <img src={gxVaultIcon} alt="" />
        </div>
      </div>
    </div>
  );
};

export default ChatPanel;
