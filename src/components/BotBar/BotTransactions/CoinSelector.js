import React, { useContext } from 'react';
import { AppContext } from '../../../contexts/AppContext';

const CoinSelector = ({ setSelectedCoin }) => {
  const { walletCoinData } = useContext(AppContext);

  return (
    <>
      <div className="header-container">
        <div className="header-text">Select Coin</div>
        <div onClick={() => setSelectedCoin('All')} className="header-action">
          See All
        </div>
      </div>
      <div className="coin-selector-container">
        {walletCoinData?.map((item) => (
          <div
            className="coin-item"
            key={item.coinSymbol}
            onClick={() => setSelectedCoin(item)}
          >
            <img src={item.coinImage} alt="" className="coin-icon" />
            <div className="coin-name">{item.coinName}</div>
          </div>
        ))}
      </div>
    </>
  );
};

export default CoinSelector;
