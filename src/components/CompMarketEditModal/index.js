import { useEffect, useRef, useState } from 'react';
import PopupModalLayout from '../../layouts/PopupModalLayout';
import axios from 'axios';
import { GX_API_ENDPOINT } from '../../configs';
import LoadingAnimation from '../LoadingAnimation';
import CompLawEdit from './CompLawEdit';
import CompBlockEdit from './CompBlockEdit';
import CompItemEdit from './CompItemEdit';
import CompChainEdit from './CompChainEdit';
import CompPlanEdit from './CompPlanEdit';

const CompMarketEditModal = ({
  isOpen,
  onClose,
  activeCategory,
  editObject,
  reloadData,
}) => {
  const [activeTab, setActiveTab] = useState();
  const [compItems, setCompItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const tabsRef = useRef();

  useEffect(() => {
    if (tabsRef.current && activeTab?.title === 'CompPlans') {
      tabsRef.current.scrollLeft = 175 * 3;
    }
  }, [activeTab, editObject]);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/fields`)
      .then(({ data }) => {
        const items = data?.fields || [];

        const parsed = items.map((item) => ({
          ...item,
          createdBy: item.collection,
        }));

        setCompItems(parsed);
      })
      .catch((error) => {});
  }, []);

  useEffect(() => {
    if (!isOpen) {
      setIsLoading(false);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && activeCategory) {
      const item = TABS.find((x) => x.title === activeCategory.title);

      setActiveTab(item);
    }
  }, [activeCategory, isOpen]);

  const renderFragement = () => {
    let fragment = null;

    switch (activeTab?.title) {
      case 'CompItems':
        fragment = (
          <CompItemEdit
            activeTab={activeTab}
            compItems={compItems}
            onClose={onClose}
            setIsLoading={setIsLoading}
            editObject={editObject}
            reloadData={reloadData}
          />
        );
        break;
      case 'CompLaws':
        fragment = (
          <CompLawEdit
            activeTab={activeTab}
            compItems={compItems}
            onClose={onClose}
            setIsLoading={setIsLoading}
            editObject={editObject}
            reloadData={reloadData}
          />
        );
        break;
      case 'CompBlocks':
        fragment = (
          <CompBlockEdit
            activeTab={activeTab}
            onClose={onClose}
            setIsLoading={setIsLoading}
            editObject={editObject}
            reloadData={reloadData}
          />
        );
        break;
      case 'CompChains':
        fragment = (
          <CompChainEdit
            activeTab={activeTab}
            compItems={compItems}
            onClose={onClose}
            setIsLoading={setIsLoading}
            editObject={editObject}
            reloadData={reloadData}
          />
        );
        break;
      case 'CompPlans':
        fragment = (
          <CompPlanEdit
            activeTab={activeTab}
            compItems={compItems}
            onClose={onClose}
            setIsLoading={setIsLoading}
            editObject={editObject}
            reloadData={reloadData}
          />
        );
        break;
      default:
        fragment = null;
        break;
    }

    return fragment;
  };

  return (
    <PopupModalLayout
      isOpen={isOpen}
      onClose={onClose}
      noHeader
      style={{ padding: 0 }}
      width={700}
    >
      <div className="add-comp-market-modal">
        {isLoading && (
          <div className="loading-container">
            <LoadingAnimation />
          </div>
        )}
        <div className="tabs-container" ref={tabsRef}>
          {TABS.map((item) => (
            <div
              key={item.title}
              className={`tab-item ${
                item.title === activeTab?.title ? 'active' : ''
              }`}
            >
              {item.title}
            </div>
          ))}
        </div>
        <div className="popup-fragment-container">{renderFragement()}</div>
      </div>
    </PopupModalLayout>
  );
};

export default CompMarketEditModal;

const TABS = [
  { title: 'CompItems', name: 'Item' },
  { title: 'CompLaws', name: 'Law' },
  { title: 'CompBlocks', name: 'Block' },
  { title: 'CompChains', name: 'Chain' },
  { title: 'CompPlans', name: 'Plan' },
];
