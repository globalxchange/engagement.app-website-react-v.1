const CompItemSelector = ({
  compItems,
  selctedCompItem,
  setSelctedCompItem,
  onBack,
  activeTab,
  title = 'Select The CompItem',
  breadcrumbName = 'Select CompItem',
}) => (
  <div className="comp-selector-container">
    <div className="title">{title}</div>
    <div className="bread-crumbs-container">
      <div className="bread-crumb-item" onClick={onBack}>
        <div className="item-name">{activeTab?.title}</div>
        <span className="arrow">{'->'}</span>
      </div>
      <div className="bread-crumb-item" onClick={onBack}>
        <div className="item-name">{breadcrumbName}</div>
      </div>
    </div>
    <div className="comp-select-items-list-container">
      {compItems?.map((item, index) => (
        <div
          key={index}
          className={`comptroller-item ${
            selctedCompItem?._id === item._id ||
            selctedCompItem?.name === item.name
              ? 'active'
              : ''
          } `}
          onClick={() => {
            setSelctedCompItem(item);
            onBack();
          }}
        >
          <img src={item.icon} alt="" className="comptroller-img" />
          <div className="item-name text-capitalize">{item.name}</div>
          <div className="item-created-by">{item.createdBy}</div>
        </div>
      ))}
    </div>
  </div>
);

export default CompItemSelector;
