const DeleteView = ({ onDelete, name }) => (
  <div className="delete-container">
    <div className="delete-message">
      Are You Sure You Want To Delete This {name}?
    </div>
    <div className="delete-button" onClick={onDelete}>
      Yes
    </div>
  </div>
);

export default DeleteView;
