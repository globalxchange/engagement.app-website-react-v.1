import { useEffect, useRef, useState } from 'react';
import {
  BRAIN_API_ENDPOINT,
  BRAIN_PATH,
  BRAIN_SECRET,
  EMAIL_DEV,
  GX_API_ENDPOINT,
} from '../../../configs';
import JsonWebToken from 'jsonwebtoken';
import { renameFile } from '../../../utils';
import axios from 'axios';
import { toast } from 'react-toastify';
import LocalStorageHelper from '../../../utils/LocalStorageHelper';
import CompItemSelector from '../CompItemSelector';
import DeleteView from '../DeleteView';

const CompBlockEdit = ({
  activeTab,
  onClose,
  setIsLoading,
  reloadData,
  editObject,
}) => {
  const [imageLink, setImageLink] = useState('');
  const [isUploading, setIsUploading] = useState(false);
  const [itemName, setItemName] = useState('');
  const [itemDesc, setItemDesc] = useState('');
  const [levelInput, setLevelInput] = useState('');
  const [levelPercentage, setLevelPercentage] = useState('');
  const [complLaws, setComplLaws] = useState([]);
  const [isCompSelectorOpen, setIsCompSelectorOpen] = useState(false);
  const [isDeleteShow, setIsDeleteShow] = useState(false);
  const [isDeletSuccess, setIsDeletSuccess] = useState(false);

  const lawSelectorCallback = useRef(() => null);

  useEffect(() => {
    if (editObject) {
      // console.log('editObject', editObject);
      setItemName(editObject.name);
      setItemDesc(editObject.description);
      setImageLink(editObject.icon);
    }
  }, [editObject]);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/params`)
      .then(({ data }) => {
        const items = data?.params || [];

        const parsed = items.map((item) => ({
          ...item,
          name: JSON.stringify(item.value),
          createdBy: `Applied To ${item.applied_to}`,
        }));

        setComplLaws(parsed);
      })
      .catch((error) => {});
  }, []);

  const onFileChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      setIsUploading(true);
      setImageLink('');
      const fileName = `${new Date().getTime()}${selectedFile.name.substr(
        selectedFile.name.lastIndexOf('.'),
      )}`;
      const file = renameFile(selectedFile, fileName);

      const data = new FormData();
      data.append('files', file);

      const token = JsonWebToken.sign(
        { name: fileName, email: EMAIL_DEV },
        BRAIN_SECRET,
        {
          algorithm: 'HS512',
          expiresIn: 240,
          issuer: 'gxjwtenchs512',
        },
      );

      axios
        .post(`${BRAIN_API_ENDPOINT}/file/dev-upload-file`, data, {
          params: { email: EMAIL_DEV, path: BRAIN_PATH, token, name: fileName },
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then(({ data }) => {
          setImageLink(data.payload.url);
        })
        .catch((err) => {
          console.log('Error on uploding', err);
        })
        .finally(() => {
          setIsUploading(false);
        });
    }
  };

  const onPublishClick = () => {
    if (!imageLink) {
      return toast.error('Please Select An Image');
    }
    if (!itemName.trim()) {
      return toast.error(`Please Input CompBlock Name`);
    }
    if (!itemDesc.trim()) {
      return toast.error(`Please Input CompBlock Description`);
    }

    const postData = {
      email: LocalStorageHelper.getAppEmail(),
      token: LocalStorageHelper.getAppToken(),
      comp_block_id: editObject?.comp_block_id,
      name: itemName.trim(),
      icon: imageLink,
      description: itemDesc.trim(),
    };

    setIsLoading(true);

    axios
      .post(`${GX_API_ENDPOINT}/coin/vault/service/edit/comp/block`, postData)
      .then(({ data }) => {
        console.log('Resp', data);
        if (data.status) {
          toast.success('CompBlock Block Edit');
          reloadData();
          onClose();
        } else {
          toast.error(data.message || 'Error Occured On Editing Law');
        }
      })
      .catch((error) => {
        console.log('Error on updating', error);
        toast.error('Network Error');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const onDelete = () => {
    setIsLoading(true);

    const postData = {
      email: LocalStorageHelper.getAppEmail(),
      token: LocalStorageHelper.getAppToken(),
      comp_block_id: editObject?.comp_block_id,
    };

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/vault/service/delete/comp/blocks`,
        postData,
      )
      .then(({ data }) => {
        console.log('Resp', data);
        if (data.status) {
          // toast.success('CompLaw Updated');
          reloadData();
          setIsDeletSuccess(true);
          // onClose();
        } else {
          toast.error(data.message || 'Error Occured On Deleting Block');
        }
      })
      .catch((error) => {
        console.log('Error on updating', error);
        toast.error('Network Error');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return isDeletSuccess ? (
    <>
      <div className="add-form-container">
        <div className="delete-success-message">
          You Have Successfully Delete {itemName}
        </div>
      </div>
      <div className="action-container">
        <div className="action-item" onClick={onClose}>
          CompLaws
        </div>
        <div className="action-item primary" onClick={onClose}>
          Compensation
        </div>
      </div>
    </>
  ) : (
    <>
      <div className="add-form-container comp-block-add">
        {isCompSelectorOpen && (
          <div className="comp-selector-wrapper">
            <CompItemSelector
              compItems={complLaws}
              selctedCompItem=""
              setSelctedCompItem={lawSelectorCallback.current}
              activeTab={activeTab}
              onBack={() => setIsCompSelectorOpen(false)}
            />
          </div>
        )}
        <div
          className="image-item"
          style={imageLink || isUploading ? { background: 'white' } : {}}
        >
          {imageLink ? (
            <>
              <img src={imageLink} alt="" className="image-preview" />
              {isDeleteShow || (
                <input
                  type="file"
                  className="drag-input mt-0 position-absolute"
                  accept="image/png, image/gif, image/jpeg"
                  onChange={onFileChange}
                  style={{ display: isUploading ? 'none' : 'block' }}
                />
              )}
            </>
          ) : (
            <>
              {isDeleteShow || (
                <input
                  type="file"
                  className="drag-input mt-0"
                  accept="image/png, image/gif, image/jpeg"
                  onChange={onFileChange}
                  style={{ display: isUploading ? 'none' : 'block' }}
                />
              )}
            </>
          )}
          {isUploading && <div className="loading-text">Uploading Photo</div>}
        </div>
        <div className="input-wrapper">
          {isDeleteShow || (
            <div className="edit-button">
              <img
                src={
                  require('../../../assets/images/edit-dark-icon.svg').default
                }
                alt=""
                className="edit-icon"
              />
            </div>
          )}
          <input
            type="text"
            className="border-less-input item-name m-0"
            placeholder="Enter Block Name"
            value={itemName}
            onChange={(e) => setItemName(e.target.value)}
          />
        </div>
        <div className="input-wrapper">
          {isDeleteShow || (
            <div className="edit-button">
              <img
                src={
                  require('../../../assets/images/edit-dark-icon.svg').default
                }
                alt=""
                className="edit-icon"
              />
            </div>
          )}
          <input
            type="text"
            className="border-less-input item-desc m-0"
            placeholder="Enter Description Here"
            value={itemDesc}
            onChange={(e) => setItemDesc(e.target.value)}
          />
        </div>
        {isDeleteShow ? (
          <DeleteView onDelete={onDelete} name="CompBlock" />
        ) : (
          <div className="comp-items-list-container" style={{ opacity: 0.3 }}>
            <div className="comp-row">
              <div className="comp-item-container mr-5">
                <div className="comp-item-title">Level</div>
                <div className="comp-item-details">
                  <input
                    disabled
                    type="number"
                    className="comp-item-input"
                    placeholder="Enter Level Number"
                    value={levelInput}
                    onWheel={(event) => event.currentTarget.blur()}
                    onChange={(e) => setLevelInput(e.target.value)}
                  />
                </div>
              </div>
              <div className="comp-item-container">
                <div className="comp-item-title">Percentage For Level</div>
                <div className="comp-item-details">
                  <input
                    disabled
                    type="number"
                    className="comp-item-input"
                    placeholder="Ex. 25%"
                    value={levelPercentage}
                    onWheel={(event) => event.currentTarget.blur()}
                    onChange={(e) => setLevelPercentage(e.target.value)}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="action-container">
        <div
          className={`action-item ${isDeleteShow ? 'primary' : ''}`}
          onClick={() => setIsDeleteShow(true)}
        >
          Delete CompBlock
        </div>
        <div
          className={`action-item ${isDeleteShow ? '' : 'primary'}`}
          onClick={isDeleteShow ? () => setIsDeleteShow(false) : onPublishClick}
        >
          Edit CompBlock
        </div>
      </div>
    </>
  );
};

const PlusButton = ({ onClick }) => (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    onClick={onClick}
  >
    <path
      d="M14 7C14 7.18565 13.9263 7.3637 13.795 7.49497C13.6637 7.62625 13.4857 7.7 13.3 7.7H7.7V13.3C7.7 13.4857 7.62625 13.6637 7.49497 13.795C7.3637 13.9263 7.18565 14 7 14C6.81435 14 6.6363 13.9263 6.50503 13.795C6.37375 13.6637 6.3 13.4857 6.3 13.3V7.7H0.7C0.514348 7.7 0.336301 7.62625 0.205025 7.49497C0.0737498 7.3637 0 7.18565 0 7C0 6.81435 0.0737498 6.6363 0.205025 6.50503C0.336301 6.37375 0.514348 6.3 0.7 6.3H6.3V0.7C6.3 0.514348 6.37375 0.336301 6.50503 0.205025C6.6363 0.0737498 6.81435 0 7 0C7.18565 0 7.3637 0.0737498 7.49497 0.205025C7.62625 0.336301 7.7 0.514348 7.7 0.7V6.3H13.3C13.4857 6.3 13.6637 6.37375 13.795 6.50503C13.9263 6.6363 14 6.81435 14 7Z"
      fill="#383C41"
    />
  </svg>
);

export default CompBlockEdit;

const RadioButton = ({
  title,
  options = [],
  value,
  onChange,
  creditPercentage,
  setCreditPercentage,
}) => (
  <div className="input-group radio-button-container">
    <div className="input-group-title">{title}</div>
    <div className="options-container">
      {options.map((item) => (
        <div
          key={item.value}
          className={`option-item ${
            value?.value === item.value ? 'active' : ''
          }`}
          onClick={() => onChange(item)}
        >
          {item.title}
        </div>
      ))}
      {value?.value === false && (
        <input
          type="number"
          className="optional-input"
          onWheel={(event) => event.currentTarget.blur()}
          value={creditPercentage}
          onChange={(e) => setCreditPercentage(e.target.value)}
          placeholder="0.00%"
        />
      )}
    </div>
  </div>
);
