/* eslint-disable jsx-a11y/no-onchange */
import axios from 'axios';
import { useEffect, useState } from 'react';
import {
  BRAIN_API_ENDPOINT,
  BRAIN_PATH,
  BRAIN_SECRET,
  EMAIL_DEV,
  GX_API_ENDPOINT,
} from '../../configs';
import { renameFile } from '../../utils';
import JsonWebToken from 'jsonwebtoken';
import LocalStorageHelper from '../../utils/LocalStorageHelper';
import { toast } from 'react-toastify';
import CompItemSelector from './CompItemSelector';
import DeleteView from './DeleteView';

const CompPlanEdit = ({
  activeTab,
  setIsLoading,
  onClose,
  compItems,
  editObject,
  reloadData,
}) => {
  const [isUploading, setIsUploading] = useState(false);
  const [imageLink, setImageLink] = useState('');
  const [isCompSelectorOpen, setIsCompSelectorOpen] = useState(false);
  const [selctedCompItem, setSelctedCompItem] = useState('');
  const [itemName, setItemName] = useState('');
  const [itemDesc, setItemDesc] = useState('');
  const [claueInput, setClaueInput] = useState(null);
  const [isDeleteShow, setIsDeleteShow] = useState(false);
  const [isDeletSuccess, setIsDeletSuccess] = useState(false);

  useEffect(() => {
    if (editObject) {
      // console.log('editObject', editObject);
      setItemName(editObject.name);
      setItemDesc(editObject.description);
      setImageLink(editObject.icon);
    }
  }, [editObject]);

  const onFileChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      setIsUploading(true);
      setImageLink('');
      const fileName = `${new Date().getTime()}${selectedFile.name.substr(
        selectedFile.name.lastIndexOf('.'),
      )}`;
      const file = renameFile(selectedFile, fileName);

      const formData = new FormData();
      formData.append('files', file);

      const token = JsonWebToken.sign(
        { name: fileName, email: EMAIL_DEV },
        BRAIN_SECRET,
        {
          algorithm: 'HS512',
          expiresIn: 240,
          issuer: 'gxjwtenchs512',
        },
      );

      axios
        .post(`${BRAIN_API_ENDPOINT}/file/dev-upload-file`, formData, {
          params: { email: EMAIL_DEV, path: BRAIN_PATH, token, name: fileName },
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then(({ data }) => {
          setImageLink(data.payload.url);
        })
        .catch((err) => {
          console.log('Error on uploding', err);
        })
        .finally(() => {
          setIsUploading(false);
        });
    }
  };

  const onPublishClick = () => {};

  const onDelete = () => {
    setIsLoading(true);

    const postData = {
      email: LocalStorageHelper.getAppEmail(),
      token: LocalStorageHelper.getAppToken(),
      comp_plan_id: editObject?.comp_plan_id,
    };

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/vault/service/delete/comp5/product/data`,
        postData,
      )
      .then(({ data }) => {
        console.log('Resp', data);
        if (data.status) {
          // toast.success('CompItem Updated');
          reloadData();
          setIsDeletSuccess(true);
          // onClose();
        } else {
          toast.error(data.message || 'Error Occured On Addin Law');
        }
      })
      .catch((error) => {
        console.log('Error on updating', error);
        toast.error('Network Error');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return isDeletSuccess ? (
    <>
      <div className="add-form-container">
        <div className="delete-success-message">
          You Have Successfully Delete {itemName}
        </div>
      </div>
      <div className="action-container">
        <div className="action-item" onClick={onClose}>
          CompPlans
        </div>
        <div className="action-item primary" onClick={onClose}>
          Compensation
        </div>
      </div>
    </>
  ) : (
    <>
      <div className="add-form-container">
        {isCompSelectorOpen ? (
          <CompItemSelector
            compItems={compItems}
            selctedCompItem={selctedCompItem}
            setSelctedCompItem={setSelctedCompItem}
            activeTab={activeTab}
            onBack={() => setIsCompSelectorOpen(false)}
          />
        ) : (
          <>
            <div
              className="image-item"
              style={imageLink || isUploading ? { background: 'white' } : {}}
            >
              {imageLink ? (
                <>
                  <img src={imageLink} alt="" className="image-preview" />
                  {isDeleteShow || (
                    <input
                      type="file"
                      className="drag-input mt-0 position-absolute"
                      accept="image/png, image/gif, image/jpeg"
                      onChange={onFileChange}
                      style={{ display: isUploading ? 'none' : 'block' }}
                    />
                  )}
                </>
              ) : (
                <>
                  {isDeleteShow || (
                    <input
                      type="file"
                      className="drag-input mt-0"
                      accept="image/png, image/gif, image/jpeg"
                      onChange={onFileChange}
                      style={{ display: isUploading ? 'none' : 'block' }}
                    />
                  )}
                </>
              )}
              {isUploading && (
                <div className="loading-text">Uploading Photo</div>
              )}
              {isDeleteShow || (
                <div className="edit-button">
                  <img
                    src={
                      require('../../assets/images/edit-dark-icon.svg').default
                    }
                    alt=""
                    className="edit-icon"
                  />
                </div>
              )}
            </div>
            <div className="input-wrapper">
              {isDeleteShow || (
                <div className="edit-button">
                  <img
                    src={
                      require('../../assets/images/edit-dark-icon.svg').default
                    }
                    alt=""
                    className="edit-icon"
                  />
                </div>
              )}
              <input
                disabled
                type="text"
                className="border-less-input item-name m-0"
                placeholder={`Enter ${activeTab?.name} Name`}
                value={itemName}
                onChange={(e) => setItemName(e.target.value)}
              />
            </div>
            <div className="input-wrapper">
              {isDeleteShow || (
                <div className="edit-button">
                  <img
                    src={
                      require('../../assets/images/edit-dark-icon.svg').default
                    }
                    alt=""
                    className="edit-icon"
                  />
                </div>
              )}
              <input
                disabled
                type="text"
                className="border-less-input item-desc mt-0"
                placeholder="Enter Description Here"
                value={itemDesc}
                onChange={(e) => setItemDesc(e.target.value)}
              />
            </div>
            {isDeleteShow ? (
              <DeleteView onDelete={onDelete} name="CompPlan" />
            ) : (
              <div
                className="comp-items-list-container"
                style={{ opacity: 0.3 }}
              >
                <div className="comp-row">
                  <div className="comp-item-container mr-5">
                    <div className="comp-item-title">Select CompPlan</div>
                    <div className="comp-item-details">
                      <img
                        src={selctedCompItem?.icon}
                        alt=""
                        className="comp-item-icon"
                      />
                      <div className="comp-item-name">
                        {selctedCompItem?.name || 'CompPlans Name'}
                      </div>
                    </div>
                  </div>
                  <div className="comp-item-container">
                    <div className="comp-item-title">Enter Clause</div>
                    <div className="comp-item-details">
                      <select
                        disabled={!selctedCompItem}
                        type="text"
                        className="comp-clause"
                        placeholder="Type Here"
                        value={claueInput}
                        onChange={(e) => setClaueInput(e.target.value)}
                      >
                        <option value="">Select A Value</option>
                        {selctedCompItem?.values?.map((item) => (
                          <option key={item} value={item}>
                            {JSON.stringify(item)}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </>
        )}
      </div>
      <div className="action-container">
        <div
          className={`action-item ${isDeleteShow ? 'primary' : ''}`}
          onClick={() => setIsDeleteShow(true)}
        >
          Delete CompPlan
        </div>
        <div
          className={`action-item ${isDeleteShow ? '' : 'primary'}`}
          onClick={isDeleteShow ? () => setIsDeleteShow(false) : onPublishClick}
        >
          Edit CompPlan
        </div>
      </div>
    </>
  );
};

export default CompPlanEdit;
