import React, { useContext, useEffect, useState } from 'react';
import { GX_API_ENDPOINT } from '../../configs';
import { AppContext } from '../../contexts/AppContext';
import LocalStorageHelper from '../../utils/LocalStorageHelper';
import axios from 'axios';
import Skeleton from 'react-loading-skeleton';

const AppsList = () => {
  const { activeOperatorApp, setActiveOperatorApp, activeBrand } = useContext(
    AppContext,
  );

  const [appsList, setAppsList] = useState();

  useEffect(() => {
    const email = LocalStorageHelper.getAppEmail();
    const brand = activeBrand || LocalStorageHelper.getLoggedBrandData();

    const apps = brand?.app_data;

    if (apps) {
      setAppsList(apps);

      if (!apps.includes(activeOperatorApp)) {
        setActiveOperatorApp(apps[0] || '');
      }
    } else {
      axios
        .get(`${GX_API_ENDPOINT}/gxb/app/gxlive/user/operator/get`, {
          params: { email, show_apps: true, id: brand?.operator_id },
        })
        .then(({ data }) => {
          // console.log('Apps List ', data);

          const operator = data?.operators ? data?.operators[0] : '';
          const appList = operator?.app_data || [];

          setAppsList(appList);
          setActiveOperatorApp(appList[0] || '');
        })
        .catch(error => {
          console.log('Error on getting app list', error);
        });
    }
  }, [activeBrand]);

  return (
    <div className="d-flex flex-fill">
      <div className="apps-container">
        {appsList ? (
          appsList?.length > 0 ? (
            appsList.map((item, index) => (
              <div
                key={index}
                className={`app-item ${
                  activeOperatorApp?._id === item._id ? 'active' : ''
                } `}
                onClick={() => setActiveOperatorApp(item)}
              >
                <img src={item.app_icon} alt="" className="app-img" />
                <div className="app-name">{item.app_name}</div>
              </div>
            ))
          ) : (
            <div className="empty-message">No Operator Apps Found</div>
          )
        ) : (
          Array(5)
            .fill(1)
            .map((_, index) => (
              <div key={index} className="app-item skeleton">
                <Skeleton width={30} height={30} circle />
                <Skeleton width={60} height={10} className="app-name" />
              </div>
            ))
        )}
      </div>
    </div>
  );
};

export default AppsList;
