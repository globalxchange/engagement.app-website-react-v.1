/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import AppsList from './AppsList';

const MyProfileCover = ({ children }) => {
  const [navHeight, setNavHeight] = useState(0);

  const navContainerRef = useRef(null);

  useLayoutEffect(() => {
    if (navContainerRef.current) {
      const height = navContainerRef.current.clientHeight;
      setNavHeight(height);
    }
  }, [
    navContainerRef.current,
    navContainerRef.current ? navContainerRef.current.clientHeight : 0,
  ]);

  return (
    <div className="broker-app-cover-wrapper">
      <AppsList />
      <div className="" ref={navContainerRef}>
        {children}
      </div>
    </div>
  );
};

export default MyProfileCover;
