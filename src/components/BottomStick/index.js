import React from 'react';

const BottomStick = () => {
  return (
    <div className="btom-stick">
      <div className="container">
        <h6 className="desk-only-dis">
          Engagement Is Now Currency. Learn How Your Business Can Automate Its
          Engagement Strategy
        </h6>
        <div className="bt-stk-btn btn">Free Webinar</div>
        <h6 className="mob-only-dis">Automate Your Engagement Today</h6>
      </div>
    </div>
  );
};

export default BottomStick;
