import React from 'react';

const ButtonLoadingAnimation = ({ dark }) => (
  <div className="loading-wrapper">
    <div className={`loader ${dark ? 'dark' : ''}`}>
      <div />
      <div>
        <div />
      </div>
    </div>
  </div>
);

export default ButtonLoadingAnimation;
