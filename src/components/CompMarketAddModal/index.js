import { useEffect, useState } from 'react';
import PopupModalLayout from '../../layouts/PopupModalLayout';
import axios from 'axios';
import { GX_API_ENDPOINT } from '../../configs';
import LoadingAnimation from '../LoadingAnimation';
import CompLawAdd from './CompLawAdd';
import CompBlockAdd from './CompBlockAdd';

const CompMarketAddModal = ({ isOpen, onClose, activeCategory }) => {
  const [activeTab, setActiveTab] = useState();
  const [compItems, setCompItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/fields`)
      .then(({ data }) => {
        const items = data?.fields || [];

        const parsed = items.map((item) => ({
          ...item,
          createdBy: item.collection,
        }));

        setCompItems(parsed);
      })
      .catch((error) => {});
  }, []);

  useEffect(() => {
    if (!isOpen) {
      setIsLoading(false);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && activeCategory) {
      const item = TABS.find((x) => x.title === activeCategory.title);

      setActiveTab(item);
    }
  }, [activeCategory, isOpen]);

  const renderFragement = () => {
    let fragment = null;

    switch (activeTab?.title) {
      case 'CompItems':
        break;
      case 'CompLaws':
        fragment = (
          <CompLawAdd
            activeTab={activeTab}
            compItems={compItems}
            onClose={onClose}
            setIsLoading={setIsLoading}
          />
        );
        break;
      case 'CompBlocks':
        fragment = (
          <CompBlockAdd
            activeTab={activeTab}
            onClose={onClose}
            setIsLoading={setIsLoading}
          />
        );
        break;
      case 'CompChains':
        break;
      case 'CompPlans':
        break;
      default:
        fragment = null;
        break;
    }

    return fragment;
  };

  return (
    <PopupModalLayout
      isOpen={isOpen}
      onClose={onClose}
      noHeader
      style={{ padding: 0 }}
      width={700}
    >
      <div className="add-comp-market-modal">
        {isLoading && (
          <div className="loading-container">
            <LoadingAnimation />
          </div>
        )}
        <div className="tabs-container">
          {TABS.map((item) => (
            <div
              key={item.title}
              className={`tab-item ${
                item.title === activeTab?.title ? 'active' : ''
              }`}
              onClick={() => setActiveTab(item)}
            >
              {item.title}
            </div>
          ))}
        </div>
        <div className="popup-fragment-container">{renderFragement()}</div>
      </div>
    </PopupModalLayout>
  );
};

export default CompMarketAddModal;

const TABS = [
  { title: 'CompItems', name: 'Item' },
  { title: 'CompLaws', name: 'Law' },
  { title: 'CompBlocks', name: 'Block' },
  { title: 'CompChains', name: 'Chain' },
  { title: 'CompPlans', name: 'Plan' },
];
