/* eslint-disable jsx-a11y/no-onchange */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { renameFile } from '../../utils';
import CompItemSelector from './CompItemSelector';
import JsonWebToken from 'jsonwebtoken';
import {
  BRAIN_API_ENDPOINT,
  BRAIN_PATH,
  BRAIN_SECRET,
  EMAIL_DEV,
  GX_API_ENDPOINT,
} from '../../configs';
import axios from 'axios';
import LocalStorageHelper from '../../utils/LocalStorageHelper';

const CompLawAdd = ({ activeTab, setIsLoading, onClose, compItems }) => {
  const [isUploading, setIsUploading] = useState(false);
  const [imageLink, setImageLink] = useState('');
  const [isCompSelectorOpen, setIsCompSelectorOpen] = useState(false);
  const [selctedCompItem, setSelctedCompItem] = useState('');
  const [itemName, setItemName] = useState('');
  const [itemDesc, setItemDesc] = useState('');
  const [claueInput, setClaueInput] = useState(null);
  const [appliesTo, setAppliesTo] = useState('');

  useEffect(() => {
    setClaueInput(null);
  }, [selctedCompItem]);

  const onFileChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      setIsUploading(true);
      const fileName = `${new Date().getTime()}${selectedFile.name.substr(
        selectedFile.name.lastIndexOf('.'),
      )}`;
      const file = renameFile(selectedFile, fileName);

      const formData = new FormData();
      formData.append('files', file);

      const token = JsonWebToken.sign(
        { name: fileName, email: EMAIL_DEV },
        BRAIN_SECRET,
        {
          algorithm: 'HS512',
          expiresIn: 240,
          issuer: 'gxjwtenchs512',
        },
      );

      axios
        .post(`${BRAIN_API_ENDPOINT}/file/dev-upload-file`, formData, {
          params: { email: EMAIL_DEV, path: BRAIN_PATH, token, name: fileName },
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then(({ data }) => {
          setImageLink(data.payload.url);
        })
        .catch((err) => {
          console.log('Error on uploding', err);
        })
        .finally(() => {
          setIsUploading(false);
        });
    }
  };

  const onPublishClick = () => {
    if (!imageLink) {
      return toast.error('Please Select An Image');
    }
    if (!itemName.trim()) {
      return toast.error(`Please Input ${activeTab?.name} Name`);
    }
    if (!itemDesc.trim()) {
      return toast.error(`Please Input ${activeTab?.name} Description`);
    }
    if (!appliesTo) {
      return toast.error('Please Select A Applying OPtion');
    }
    if (claueInput === null || claueInput === '') {
      return toast.error(`Please Input ${activeTab?.name} Clause`);
    }

    setIsLoading(true);

    const postData = {
      email: LocalStorageHelper.getAppEmail(),
      token: LocalStorageHelper.getAppToken(),
      name: itemName.trim(),
      icon: imageLink,
      description: itemDesc.trim(),
      field_id: selctedCompItem.field_id,
      value: JSON.parse(claueInput),
      applied_to: appliesTo.value,
    };

    console.log('postData', postData);

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/vault/service/define/comp/params`,
        postData,
      )
      .then(({ data }) => {
        console.log('Resp', data);
        if (data.status) {
          toast.success('New CompLaw Added');
          onClose();
        } else {
          toast.error(data.message || 'Error Occured On Addin Law');
        }
      })
      .catch((error) => {
        console.log('Error on updating', error);
        toast.error('Network Error');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <div className="add-form-container">
        {isCompSelectorOpen ? (
          <CompItemSelector
            compItems={compItems}
            selctedCompItem={selctedCompItem}
            setSelctedCompItem={setSelctedCompItem}
            activeTab={activeTab}
            onBack={() => setIsCompSelectorOpen(false)}
          />
        ) : (
          <>
            <div
              className="image-item"
              style={imageLink || isUploading ? { background: 'white' } : {}}
            >
              {imageLink ? (
                <>
                  <img src={imageLink} alt="" className="image-preview" />
                </>
              ) : (
                <input
                  type="file"
                  className="drag-input"
                  accept="image/png, image/gif, image/jpeg"
                  onChange={onFileChange}
                  style={{ display: isUploading ? 'none' : 'block' }}
                />
              )}
              {isUploading && (
                <div className="loading-text">Uploading Photo</div>
              )}
            </div>
            <input
              type="text"
              className="border-less-input item-name"
              placeholder={`Enter ${activeTab?.name} Name`}
              value={itemName}
              onChange={(e) => setItemName(e.target.value)}
            />
            <input
              type="text"
              className="border-less-input item-desc"
              placeholder="Enter Description Here"
              value={itemDesc}
              onChange={(e) => setItemDesc(e.target.value)}
            />
            <div className="appiles-to-container">
              <div className="appiles-to-header">
                Who Can This {activeTab?.name} Be Applied To?
              </div>
              <div className="applies-to-list">
                {APPLIES_LIST.map((item) => (
                  <div
                    key={item.value}
                    className={`applies-to-item ${
                      appliesTo?.value === item.value ? 'active' : ''
                    }`}
                    onClick={() => setAppliesTo(item)}
                  >
                    <div className="applies-to-icon-container">
                      <img src={item.icon} alt="" className="applies-to-icon" />
                    </div>
                    <div className="applies-to-name">{item.title}</div>
                  </div>
                ))}
              </div>
            </div>
            <div className="comp-items-list-container">
              <div className="comp-row">
                <div
                  className="comp-item-container mr-5"
                  onClick={() => setIsCompSelectorOpen(true)}
                >
                  <div className="comp-item-title">Select CompItems</div>
                  <div className="comp-item-details">
                    <img
                      src={selctedCompItem?.icon}
                      alt=""
                      className="comp-item-icon"
                    />
                    <div className="comp-item-name">
                      {selctedCompItem?.name || 'CompItems Name'}
                    </div>
                  </div>
                </div>
                <div className="comp-item-container">
                  <div className="comp-item-title">Enter Clause</div>
                  <div className="comp-item-details">
                    <select
                      disabled={!selctedCompItem}
                      type="text"
                      className="comp-clause"
                      placeholder="Type Here"
                      value={claueInput}
                      onChange={(e) => setClaueInput(e.target.value)}
                    >
                      <option value="">Select A Value</option>
                      {selctedCompItem?.values?.map((item) => (
                        <option key={item} value={item}>
                          {JSON.stringify(item)}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
      <div className="action-container">
        <div className="action-item" onClick={onClose}>
          Close
        </div>
        <div className="action-item primary" onClick={onPublishClick}>
          Publish {activeTab?.name}
        </div>
      </div>
    </>
  );
};

export default CompLawAdd;

const APPLIES_LIST = [
  {
    title: 'Directs',
    value: 'directs',
    icon: require('../../assets/images/assets-io-icon.svg').default,
  },
  {
    title: 'Indirects',
    value: 'indirects',
    icon: require('../../assets/images/assets-io-icon.svg').default,
  },
  {
    title: 'Affiliate',
    value: 'broker',
    icon: require('../../assets/images/assets-io-icon.svg').default,
  },
  {
    title: 'Everyone',
    value: 'uplines',
    icon: require('../../assets/images/assets-io-icon.svg').default,
  },
];
