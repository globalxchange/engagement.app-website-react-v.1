import { useState } from 'react';

const AltRequirement = ({
  pos,
  onRemove,
  onCompSelectorOpen,
  amendments,
  setAmendments,
}) => {
  const [isMinimized, setIsMinimized] = useState(false);
  const [selectedCompLaw, setSelectedCompLaw] = useState('');

  const onQulifyChange = (selected) => {
    const data = [...amendments];

    data[pos].make_qualified = selected.value;
    setAmendments(data);
  };

  const onThresholdChange = (value) => {
    const data = [...amendments];

    data[pos].number_required = parseInt(value, 10);
    setAmendments(data);
  };

  const onCompLawChange = (value) => {
    setSelectedCompLaw(value);
    const data = [...amendments];

    data[pos].param_hash = value.param_hash;
    setAmendments(data);
  };

  return (
    <div className={`item-form-container ${isMinimized ? 'saved' : ''}`}>
      <div className="form-header-container">
        <div className="menu-title" onClick={() => setIsMinimized(false)}>
          Amendment {pos + 1}
        </div>
        <img
          src={require('../../../assets/images/expand-arrow-icon.svg').default}
          alt=""
          className="expand-icon"
          onClick={() => setIsMinimized(true)}
        />
        <PlusButton onClick={onRemove} />
      </div>
      <div className="card-block p-4">
        <RadioButton
          title="Is Compliance With This Legislation Mandatory To Unlock The Amendmend For This CompBlock?"
          options={[
            { title: 'Yes', value: true },
            { title: 'No', value: false },
          ]}
          value={amendments[pos].make_qualified}
          onChange={(selected) => onQulifyChange(selected)}
        />
      </div>
      <div
        className="comp-law-row"
        onClick={() => (isMinimized ? setIsMinimized(false) : false)}
      >
        <div
          className="select-comp-law"
          onClick={() =>
            isMinimized ? null : onCompSelectorOpen(onCompLawChange)
          }
        >
          <img src={selectedCompLaw?.icon} alt="" className="comp-law-icon" />
          <div className="comp-law-name">
            {selectedCompLaw?.name || 'Select CompLaw'}
          </div>
        </div>
        <div className="qualification-input-container">
          <div className="qualification-text">Qualification Threshold</div>
          <svg
            width="14"
            height="9"
            viewBox="0 0 14 9"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <g opacity="0.25">
              <path
                d="M13.9006 4.20605L10.1023 0.11869C10.032 0.0431598 9.93342 0 9.83019 0H8.19799C7.87341 0 7.70485 0.386981 7.9259 0.624652L11.4918 4.45889L7.9259 8.29312C7.70485 8.53079 7.87341 8.91777 8.19799 8.91777H9.83019C9.93342 8.91777 10.032 8.8749 10.1023 8.79908L13.9006 4.71172C14.033 4.56912 14.033 4.34865 13.9006 4.20605Z"
                fill="#383C41"
              />
              <path
                d="M9.88916 4.20605L6.0908 0.11898C6.02052 0.0431581 5.92195 0.000289917 5.81871 0.000289917H4.18651C3.86194 0.000289917 3.69338 0.387271 3.91443 0.624942L5.94761 2.81122H0.583242C0.261292 2.81122 0 3.07252 0 3.39447V5.52418C0 5.84613 0.261292 6.10742 0.583242 6.10742H5.94732L3.91414 8.29341C3.69309 8.53108 3.86165 8.91806 4.18622 8.91806H5.81842C5.92166 8.91806 6.02023 8.87519 6.09051 8.79937L9.88887 4.71201C10.0216 4.56941 10.0216 4.34865 9.88916 4.20605Z"
                fill="#383C41"
              />
            </g>
          </svg>
          <input
            type="number"
            className="law-input"
            onWheel={(event) => event.currentTarget.blur()}
            placeholder="00"
            value={amendments[pos].number_required.toString()}
            onChange={(e) => onThresholdChange(e.target.value)}
          />
        </div>
      </div>
    </div>
  );
};

const RadioButton = ({ title, options = [], value, onChange }) => (
  <div className="input-group radio-button-container">
    <div className="input-group-title">{title}</div>
    <div className="options-container">
      {options.map((item) => (
        <div
          key={item.value}
          className={`option-item ${value === item.value ? 'active' : ''}`}
          onClick={() => onChange(item)}
        >
          {item.title}
        </div>
      ))}
    </div>
  </div>
);

const PlusButton = ({ onClick, rotate }) => (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    onClick={onClick}
    style={rotate ? { transform: 'rotate(45deg)' } : {}}
  >
    <path
      d="M14 7C14 7.18565 13.9263 7.3637 13.795 7.49497C13.6637 7.62625 13.4857 7.7 13.3 7.7H7.7V13.3C7.7 13.4857 7.62625 13.6637 7.49497 13.795C7.3637 13.9263 7.18565 14 7 14C6.81435 14 6.6363 13.9263 6.50503 13.795C6.37375 13.6637 6.3 13.4857 6.3 13.3V7.7H0.7C0.514348 7.7 0.336301 7.62625 0.205025 7.49497C0.0737498 7.3637 0 7.18565 0 7C0 6.81435 0.0737498 6.6363 0.205025 6.50503C0.336301 6.37375 0.514348 6.3 0.7 6.3H6.3V0.7C6.3 0.514348 6.37375 0.336301 6.50503 0.205025C6.6363 0.0737498 6.81435 0 7 0C7.18565 0 7.3637 0.0737498 7.49497 0.205025C7.62625 0.336301 7.7 0.514348 7.7 0.7V6.3H13.3C13.4857 6.3 13.6637 6.37375 13.795 6.50503C13.9263 6.6363 14 6.81435 14 7Z"
      fill="#383C41"
    />
  </svg>
);

export default AltRequirement;
