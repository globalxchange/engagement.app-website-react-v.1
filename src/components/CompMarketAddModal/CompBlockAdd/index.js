import { useEffect, useRef, useState } from 'react';
import {
  BRAIN_API_ENDPOINT,
  BRAIN_PATH,
  BRAIN_SECRET,
  EMAIL_DEV,
  GX_API_ENDPOINT,
} from '../../../configs';
import JsonWebToken from 'jsonwebtoken';
import { renameFile } from '../../../utils';
import axios from 'axios';
import BlockRequirements from './BlockRequirements';
import { toast } from 'react-toastify';
import LocalStorageHelper from '../../../utils/LocalStorageHelper';
import AltRequirement from './AltRequirement';
import CompItemSelector from '../CompItemSelector';
import * as Yup from 'yup';

const VALIDATION_SCHEMA = Yup.object({
  email: Yup.string().required(''),
  token: Yup.string().required(''),
  name: Yup.string().required('Please Enter CompBlock Name'),
  icon: Yup.string().required('Please Select CompBlock Icon'),
  description: Yup.string().required('Please Enter CompBlock Description'),
  level: Yup.number()
    .required('Please Input Level Number')
    .typeError('Please Input A Valid Number'),
  percentage: Yup.number()
    .required('Please Input Level Percentage')
    .typeError('Please Input A Valid Percentage'),
  requirement: Yup.array()
    .of(
      Yup.object({
        make_qualified: Yup.boolean()
          .required('Select Commition Option')
          .typeError('Select Commition Option'),
        number_required: Yup.number()
          .required('Please Input CompLaw Threshold')
          .typeError('Please Input A Valid CompLaw Threshold'),
        param_hash: Yup.string().required('Please Select A CompLaw'),
      }),
    )
    .min(1),
  not_eligible: Yup.object({
    credit_app_owner: Yup.boolean()
      .required('Select Credit Option')
      .typeError('Select Credit Option'),
    percentage_retained_by_user: Yup.number()
      .required('Please Input Level Percentage')
      .typeError('Please Input A Valid Percentage'),
    requirement: Yup.array()
      .of(
        Yup.object({
          make_qualified: Yup.boolean()
            .required('Select Commition Option')
            .typeError('Select Commition Option'),
          number_required: Yup.number()
            .required('Please Input CompLaw Threshold')
            .typeError('Please Input A Valid CompLaw Threshold'),
          param_hash: Yup.string().required('Please Select A CompLaw'),
        }),
      )
      .min(1),
  }),
});

const CompBlockAdd = ({ activeTab, onClose, setIsLoading }) => {
  const [imageLink, setImageLink] = useState('');
  const [isUploading, setIsUploading] = useState(false);
  const [itemName, setItemName] = useState('');
  const [itemDesc, setItemDesc] = useState('');
  const [levelInput, setLevelInput] = useState('');
  const [levelPercentage, setLevelPercentage] = useState('');
  const [constitutions, setConstitutions] = useState([]);
  const [amendments, setAmendments] = useState([]);
  const [isCreditOwner, setIsCreditOwner] = useState(null);
  const [creditPercentage, setCreditPercentage] = useState('');
  const [complLaws, setComplLaws] = useState([]);
  const [isCompSelectorOpen, setIsCompSelectorOpen] = useState(false);
  const [isSubmitEnabled, setIsSubmitEnabled] = useState(false);

  const lawSelectorCallback = useRef(() => null);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/coin/vault/service/get/comp/params`)
      .then(({ data }) => {
        const items = data?.params || [];

        const parsed = items.map((item) => ({
          ...item,
          name: JSON.stringify(item.value),
          createdBy: `Applied To ${item.applied_to}`,
        }));

        setComplLaws(parsed);
      })
      .catch((error) => {});
  }, []);

  useEffect(() => {
    const postData = {
      email: LocalStorageHelper.getAppEmail(),
      token: LocalStorageHelper.getAppToken(),
      name: itemName.trim(),
      icon: imageLink,
      description: itemDesc.trim(),
      level: parseInt(levelInput.trim(), 10),
      percentage: parseFloat(levelPercentage.trim()),
      requirement: constitutions,
      not_eligible: {
        credit_app_owner: isCreditOwner?.value,
        percentage_retained_by_user: parseFloat(creditPercentage.trim()) || 0,
        requirement: amendments,
      },
    };

    // console.log('postData', postData);

    VALIDATION_SCHEMA.validate(postData)
      .then(() => {
        setIsSubmitEnabled(true);
      })
      .catch((error) => {
        // console.log('Error', error);
      });
  }, [
    itemName,
    imageLink,
    itemDesc,
    levelInput,
    levelPercentage,
    constitutions,
    isCreditOwner,
    creditPercentage,
    amendments,
  ]);

  const onCompSelectorOpen = (callback) => {
    lawSelectorCallback.current = callback;
    setIsCompSelectorOpen(true);
  };

  const onAddConsttnClick = () => {
    const data = [...constitutions];
    data.push({ make_qualified: null, number_required: '', param_hash: '' });
    setConstitutions(data);
  };

  const onRemoveConsttnClick = (index) => {
    const data = [...constitutions];
    data.splice(index, 1);
    setConstitutions(data);
  };

  const onAddAmdmntClick = () => {
    const data = [...amendments];
    data.push({ make_qualified: null, number_required: '', param_hash: '' });
    setAmendments(data);
  };

  const onRemoveAmdmntClick = (index) => {
    const data = [...amendments];
    data.splice(index, 1);
    setAmendments(data);
  };

  const onFileChange = (event) => {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      setIsUploading(true);
      const fileName = `${new Date().getTime()}${selectedFile.name.substr(
        selectedFile.name.lastIndexOf('.'),
      )}`;
      const file = renameFile(selectedFile, fileName);

      const data = new FormData();
      data.append('files', file);

      const token = JsonWebToken.sign(
        { name: fileName, email: EMAIL_DEV },
        BRAIN_SECRET,
        {
          algorithm: 'HS512',
          expiresIn: 240,
          issuer: 'gxjwtenchs512',
        },
      );

      axios
        .post(`${BRAIN_API_ENDPOINT}/file/dev-upload-file`, data, {
          params: { email: EMAIL_DEV, path: BRAIN_PATH, token, name: fileName },
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        })
        .then(({ data }) => {
          setImageLink(data.payload.url);
        })
        .catch((err) => {
          console.log('Error on uploding', err);
        })
        .finally(() => {
          setIsUploading(false);
        });
    }
  };

  const onPublishClick = () => {
    if (!imageLink) {
      return toast.error('Please Select An Image');
    }
    if (!itemName.trim()) {
      return toast.error(`Please Input CompBlock Name`);
    }
    if (!itemDesc.trim()) {
      return toast.error(`Please Input CompBlock Description`);
    }
    if (!parseInt(levelInput.trim(), 10)) {
      return toast.error('Please Input Level Number');
    }
    if (!parseFloat(levelPercentage.trim())) {
      return toast.error(`Please Input Level Percentage`);
    }

    const postData = {
      email: LocalStorageHelper.getAppEmail(),
      token: LocalStorageHelper.getAppToken(),
      name: itemName.trim(),
      icon: imageLink,
      description: itemDesc.trim(),
      level: parseInt(levelInput.trim(), 10),
      percentage: parseFloat(levelPercentage.trim()),
      requirement: constitutions,
      not_eligible: {
        credit_app_owner: isCreditOwner,
        percentage_retained_by_user: parseFloat(creditPercentage.trim()),
        requirement: amendments,
      },
    };

    setIsLoading(true);

    axios
      .post(`${GX_API_ENDPOINT}/coin/vault/service/define/comp/block`, postData)
      .then(({ data }) => {
        console.log('Resp', data);
        if (data.status) {
          toast.success('New CompBlock Added');
          onClose();
        } else {
          toast.error(data.message || 'Error Occured On Addin Law');
        }
      })
      .catch((error) => {
        console.log('Error on updating', error);
        toast.error('Network Error');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <div className="add-form-container comp-block-add">
        {isCompSelectorOpen && (
          <div className="comp-selector-wrapper">
            <CompItemSelector
              compItems={complLaws}
              selctedCompItem=""
              setSelctedCompItem={lawSelectorCallback.current}
              activeTab={activeTab}
              onBack={() => setIsCompSelectorOpen(false)}
            />
          </div>
        )}
        <div
          className="image-item"
          style={imageLink || isUploading ? { background: 'white' } : {}}
        >
          {imageLink ? (
            <>
              <img src={imageLink} alt="" className="image-preview" />
            </>
          ) : (
            <input
              type="file"
              className="drag-input"
              accept="image/png, image/gif, image/jpeg"
              onChange={onFileChange}
              style={{ display: isUploading ? 'none' : 'block' }}
            />
          )}
          {isUploading && <div className="loading-text">Uploading Photo</div>}
        </div>
        <input
          type="text"
          className="border-less-input item-name"
          placeholder="Enter Block Name"
          value={itemName}
          onChange={(e) => setItemName(e.target.value)}
        />
        <input
          type="text"
          className="border-less-input item-desc"
          placeholder="Enter Description Here"
          value={itemDesc}
          onChange={(e) => setItemDesc(e.target.value)}
        />
        <div className="comp-items-list-container">
          <div className="comp-row">
            <div className="comp-item-container mr-5">
              <div className="comp-item-title">Level</div>
              <div className="comp-item-details">
                <input
                  type="number"
                  className="comp-item-input"
                  placeholder="Enter Level Number"
                  value={levelInput}
                  onWheel={(event) => event.currentTarget.blur()}
                  onChange={(e) => setLevelInput(e.target.value)}
                />
              </div>
            </div>
            <div className="comp-item-container">
              <div className="comp-item-title">Percentage For Level</div>
              <div className="comp-item-details">
                <input
                  type="number"
                  className="comp-item-input"
                  placeholder="Ex. 25%"
                  value={levelPercentage}
                  onWheel={(event) => event.currentTarget.blur()}
                  onChange={(e) => setLevelPercentage(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="main-title mt-3">CompBlock’s Constitution</div>
        <div className="main-sub-title">
          You Must Add Aleast One CompLegislation To Your CompBlock’s
          Constitution To Continue
        </div>
        {constitutions.map((item, index) => (
          <BlockRequirements
            key={index}
            pos={index}
            onRemoveClick={onRemoveConsttnClick}
            onCompSelectorOpen={onCompSelectorOpen}
            setConstitutions={setConstitutions}
            constitutions={constitutions}
          />
        ))}
        <div className="add-button-container mb-4" onClick={onAddConsttnClick}>
          <div className="add-text">
            {constitutions.length <= 0
              ? 'Click Here To Add Your First Piece Of Legislation'
              : 'Click Here To Add Another Piece Of Legislation'}
          </div>
          <PlusButton />
        </div>
        <div className="main-title mt-3">Constitution’s Amendments</div>
        <div className="main-sub-title">
          You Must Add Atleast One Amendment To Regulate Circumstances Where
          There Is A Breach Of A Mandatory Constitutional Legislatiion.
        </div>
        <RadioButton
          title="Does The App Owner Get All The Retained Commissions  When A Legislation Is Breached? If No, Fill In The Percentage That Will Be Kept By The Disqualified Constituent."
          options={[
            { title: 'Yes', value: true },
            { title: 'No', value: false },
          ]}
          value={isCreditOwner}
          onChange={(selected) => setIsCreditOwner(selected)}
          creditPercentage={creditPercentage}
          setCreditPercentage={setCreditPercentage}
        />
        {amendments.map((item, index) => (
          <AltRequirement
            key={index}
            pos={index}
            onRemove={onRemoveAmdmntClick}
            onCompSelectorOpen={onCompSelectorOpen}
            amendments={amendments}
            setAmendments={setAmendments}
          />
        ))}
        <div className="add-button-container mb-4" onClick={onAddAmdmntClick}>
          <div className="add-text">
            {amendments.length <= 0
              ? 'Click Here To Add Your First Amendment'
              : 'Click Here To Add Another Amendment'}
          </div>
          <PlusButton />
        </div>
      </div>
      <div className="action-container">
        <div className="action-item" onClick={onClose}>
          Close
        </div>
        <div
          className="action-item primary"
          onClick={isSubmitEnabled ? onPublishClick : null}
          style={{ opacity: isSubmitEnabled ? 1 : 0.4 }}
        >
          Publish Block
        </div>
      </div>
    </>
  );
};

const PlusButton = ({ onClick }) => (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    onClick={onClick}
  >
    <path
      d="M14 7C14 7.18565 13.9263 7.3637 13.795 7.49497C13.6637 7.62625 13.4857 7.7 13.3 7.7H7.7V13.3C7.7 13.4857 7.62625 13.6637 7.49497 13.795C7.3637 13.9263 7.18565 14 7 14C6.81435 14 6.6363 13.9263 6.50503 13.795C6.37375 13.6637 6.3 13.4857 6.3 13.3V7.7H0.7C0.514348 7.7 0.336301 7.62625 0.205025 7.49497C0.0737498 7.3637 0 7.18565 0 7C0 6.81435 0.0737498 6.6363 0.205025 6.50503C0.336301 6.37375 0.514348 6.3 0.7 6.3H6.3V0.7C6.3 0.514348 6.37375 0.336301 6.50503 0.205025C6.6363 0.0737498 6.81435 0 7 0C7.18565 0 7.3637 0.0737498 7.49497 0.205025C7.62625 0.336301 7.7 0.514348 7.7 0.7V6.3H13.3C13.4857 6.3 13.6637 6.37375 13.795 6.50503C13.9263 6.6363 14 6.81435 14 7Z"
      fill="#383C41"
    />
  </svg>
);

export default CompBlockAdd;

const RadioButton = ({
  title,
  options = [],
  value,
  onChange,
  creditPercentage,
  setCreditPercentage,
}) => (
  <div className="input-group radio-button-container">
    <div className="input-group-title">{title}</div>
    <div className="options-container">
      {options.map((item) => (
        <div
          key={item.value}
          className={`option-item ${
            value?.value === item.value ? 'active' : ''
          }`}
          onClick={() => onChange(item)}
        >
          {item.title}
        </div>
      ))}
      {value?.value === false && (
        <input
          type="number"
          className="optional-input"
          onWheel={(event) => event.currentTarget.blur()}
          value={creditPercentage}
          onChange={(e) => setCreditPercentage(e.target.value)}
          placeholder="0.00%"
        />
      )}
    </div>
  </div>
);
