import React from 'react';
import { Navbar, Nav, DropdownButton } from 'react-bootstrap';
import EngApp from '../../assets/EngApp.svg';
import { Link } from 'react-router-dom';
import { AppConsumer } from '../../contexts/AppContext';

const NavbarTop = () => {
  const menuItem = (
    <div
      className="d-flex align-items-center mb-4"
      style={{ minWidth: 'calc(100% / 3)' }}
    >
      <div className="menu-drop-icon">
        <i className="fas fa-paper-plane" />
      </div>
      <div style={{ maxWidth: '190px' }}>
        <h6 className="m-0" style={{ lineHeight: '1.1' }}>
          Publish
        </h6>
        <div className="m-0" style={{ lineHeight: '1.1', fontSize: '.8rem' }}>
          Manage all your content in one calendar.
        </div>
      </div>
    </div>
  );
  return (
    <AppConsumer>
      {value => {
        const { isMobMenu, setIsMobMenuOpen } = value;
        return (
          <Navbar
            collapseOnSelect
            expand="true"
            fixed="top"
            className="nav-full p-4"
          >
            <div className="container-fluid px-md-5">
              <i
                className="fas fa-bars mob-only-dis"
                style={{ fontSize: '2rem' }}
                onClick={() => setIsMobMenuOpen(true)}
              />
              <Navbar.Brand href="/" className="engapp-brand">
                <img src={EngApp} alt="no_img" />
              </Navbar.Brand>
              <Nav className="flex-row ml-auto desk-only-dis">
                <div className="drop-nav-tp">
                  <DropdownButton id="dropdown-item-button" title="Platform">
                    <div className="container">
                      <div className="drop-menu-w">
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Publish
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Engage
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Advertise
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        {menuItem}
                        {menuItem}
                        {menuItem}
                        {menuItem}
                      </div>
                    </div>
                  </DropdownButton>
                </div>
                <div className="drop-nav-tp">
                  <DropdownButton id="dropdown-item-button" title="Solutions">
                    <div className="container">
                      <div className="drop-menu-w">
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Social Media Management
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Social Listening
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Content Marketing
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        {menuItem}
                        {menuItem}
                        {menuItem}
                      </div>
                    </div>
                  </DropdownButton>
                </div>
                {/* <Nav.Link className="links-nav-b" href="/">Pricing</Nav.Link> */}
                <div className="drop-nav-tp">
                  <DropdownButton id="dropdown-item-button" title="Prices">
                    <div className="container">
                      <div className="drop-menu-w">
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Blog
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Handbooks
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center mb-4"
                          style={{ minWidth: 'calc(100% / 3)' }}
                        >
                          <div className="menu-drop-icon">
                            <i className="fas fa-paper-plane" />
                          </div>
                          <div style={{ maxWidth: '190px' }}>
                            <h6 className="m-0" style={{ lineHeight: '1.1' }}>
                              Webinars
                            </h6>
                            <div
                              className="m-0"
                              style={{ lineHeight: '1.1', fontSize: '.8rem' }}
                            >
                              Manage all your content in one calendar.
                            </div>
                          </div>
                        </div>
                        {menuItem}
                        {menuItem}
                        {menuItem}
                      </div>
                    </div>
                  </DropdownButton>
                </div>
                <div className="drop-nav-tp">
                  <DropdownButton id="dropdown-item-button" title="Academy">
                    <div className="container">
                      <div className="d-flex w-100">
                        <div style={{ width: '20%' }}>
                          <h6 className="m-0">Benefits</h6>
                          <p className="m-0">Services</p>
                          <p className="m-0">Customers</p>
                          <p className="m-0">Partners</p>
                        </div>
                        <div style={{ width: '20%' }}>
                          <h6 className="m-0">Support</h6>
                          <p className="m-0">Platform Status</p>
                          <p className="m-0">Help Center</p>
                          <p className="m-0">Report Security Issue</p>
                        </div>
                      </div>
                    </div>
                  </DropdownButton>
                </div>
                {/* <Nav.Link className="links-nav-b" href="/">Login &#8224;</Nav.Link> */}
                <Link className="nav-bt btn" to="/login">
                  Login
                </Link>
              </Nav>
              <div>
                {isMobMenu ? (
                  <div className="open-mob-menu">
                    <i
                      className="fas fa-times"
                      onClick={() => setIsMobMenuOpen(false)}
                    />
                    <div className="d-flex align-items-center justify-content-center flex-column">
                      <img
                        src={EngApp}
                        alt="no_img"
                        style={{ width: '5rem' }}
                      />
                      <nav className="mt-4 h-100 d-flex align-items-center justify-content-between flex-column">
                        <a href="/" className="menu-link">
                          Platform
                        </a>
                        <a href="/" className="menu-link">
                          Solutions
                        </a>
                        <div className="menu-link">Prices</div>
                        <a href="/" className="menu-link">
                          Academy
                        </a>
                        <Link className="nav-bt btn" to="/">
                          Login
                        </Link>
                      </nav>
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </Navbar>
        );
      }}
    </AppConsumer>
  );
};

export default NavbarTop;
