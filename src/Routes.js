import React, { useContext } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { AppContext } from './contexts/AppContext';
import AppInstallerPage from './pages/AppInstallerPage';
import CompensationHomePage from './pages/CompensationHomePage';
import CompMarketPage from './pages/CompMarketPage';
import DashboardHome from './pages/DashboardHome';
import LandingPage from './pages/LandingPage';
import LoginPage from './pages/LoginPage';
import MobileLandingPage from './pages/MobileLandingPage';
import PreRegisterPage from './pages/PreRegisterPage';
import ProductsPage from './pages/ProductsPage';
import RegistrationPage from './pages/RegistrationPage';
import SignupLandingPage from './pages/SignupLandingPage';

const Routes = () => {
  const { isMobile } = useContext(AppContext);

  return (
    <>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <Route path="/pre-registration" component={PreRegisterPage} />
        <Route exact path="/registration" component={SignupLandingPage} />
        <Route path="/mobile-landing" component={MobileLandingPage} />
        <Route path="/registration/pre" component={PreRegisterPage} />
        <Route
          path="/registration/new"
          component={() => <RegistrationPage noReferral />}
        />
        <Route path="/registration/referred" component={RegistrationPage} />
        <Route path="/dashboard" component={DashboardBoardRouter} />
        <Route path="/app-installer/:platform" component={AppInstallerPage} />
        <Route path="/app-installer/" component={AppInstallerPage} />
        <Route path="/" component={LandingPage} />
      </Switch>
      {isMobile && <Redirect to="/mobile-landing" />}
    </>
  );
};

const DashboardBoardRouter = () => (
  <BrowserRouter basename="dashboard">
    <Switch>
      <Route path="/products" component={ProductsPage} />
      <Route path="/compensation/comp-market" component={CompMarketPage} />
      <Route path="/compensation" component={CompensationHomePage} />
      <Route path="/" component={DashboardHome} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
