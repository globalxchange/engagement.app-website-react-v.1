import React from 'react';
import { Link } from 'react-router-dom';
import DashboardLayout from '../layouts/DashboardLayout';

const CompensationHomePage = () => (
  <DashboardLayout>
    <div className="endorsement-home-wrapper">
      <div
        className="cover-img"
        style={{
          backgroundImage: `url(${
            require('../assets/images/compensation-cover.jpg').default
          })`,
        }}
      />
      <div className="header-container">
        <Link to="/compensation/comp-market" className="header-action">
          CompMarket
        </Link>
        <div className="header-logo-container">
          <img
            src={require('../assets/images/copensation-full-logo.svg').default}
            alt=""
            className="header-logo"
          />
        </div>
        <Link to="/compensation/my-comp-plans" className="header-action">
          My CompPlans
        </Link>
      </div>
      <div className="content-container">
        <div className="content-title">
          Select One Of The Following Options To Get Started
        </div>
        <div className="cards-container">
          <div className="card-item-container">
            <div className="card-item-header">Watch Webinar</div>
            <div className="card-item-body">
              <div className="card-text">
                Click Here To Watch The Quick Start Training
              </div>
              <div className="card-action">Start Training</div>
            </div>
          </div>
          <div className="card-item-container">
            <div
              className="card-item-header alt"
              style={{ backgroundColor: '#26A69A', color: 'white' }}
            >
              Create CompPlan
            </div>
            <div className="card-item-body">
              <div className="card-text">
                Use Our State Of The Art CompBuilder
              </div>
              <Link to="/compensation/myplans" className="card-action">
                Create Plan
              </Link>
            </div>
          </div>
          <div className="card-item-container">
            <div className="card-item-header">Connect To Products</div>
            <div className="card-item-body">
              <div className="card-text">
                Connect Your CompPlans To Products That You Sell
              </div>
              <Link to="/compensation/myproducts" className="card-action">
                My Products
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </DashboardLayout>
);

export default CompensationHomePage;
