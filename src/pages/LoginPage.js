import React from 'react';
import LoginFrom from '../components/LoginFrom';
import AuthLayout from '../layouts/AuthLayout';

const LoginPage = () => {
  return (
    <AuthLayout>
      <LoginFrom />
    </AuthLayout>
  );
};

export default LoginPage;
