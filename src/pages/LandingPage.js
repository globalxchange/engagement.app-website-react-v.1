import React, { useEffect, useState } from 'react';
import PublishPostPreviewImg from '../assets/Publish_Post_Preview.jpg';
import PublishStatusPostImg from '../assets/Publish_Status_Post.jpg';
import PublishNotesImg from '../assets/Publish_Notes.jpg';
import EngageHoorayIcon from '../assets/Engage_Hooray.jpg';
import EngageUnreadIcon from '../assets/Engage_Unread.jpg';
import EngageProfileIcon from '../assets/Engage_Profile.jpg';
import Chat from '../assets/chat.png';
import BottomBrand1 from '../assets/bottom-brand-1.png';
import BottomBrand2 from '../assets/bottom-brand-2.png';
import BottomBrand3 from '../assets/bottom-brand-3.png';
import BottomBrand4 from '../assets/bottom-brand-4.png';
import IconAndroid from '../assets/Icon_Android.svg';
import LandingLayout from '../layouts/LandingLayout';
import dottedArrow from '../assets/images/dotter-arrow-right.svg';
import { Link } from 'react-router-dom';
import Typer from '../components/Typer';
import axios from 'axios';
import { APP_CODE, GX_API_ENDPOINT } from '../configs';

const LandingPage = () => {
  const [latestQuickCommit, setLatestQuickCommit] = useState();

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
        params: { app_code: APP_CODE },
      })
      .then(({ data }) => {
        // console.log('App Logs', data);

        const app = data.logs[0] || null;

        setLatestQuickCommit(app);
      })
      .catch(error => {});
  }, []);

  const onAppInstallerClick = isAndroid => {
    if (latestQuickCommit) {
      const link = isAndroid
        ? latestQuickCommit?.android_app_link || ''
        : latestQuickCommit?.ios_app_link || '';
      const win = window.open(link, '_blank');
      if (win != null) {
        win.focus();
      }
    } else {
      axios
        .get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
          params: { app_code: APP_CODE },
        })
        .then(({ data }) => {
          // console.log('App Logs', data);

          const app = data.logs[0] || null;

          const link = isAndroid
            ? app?.android_app_link || ''
            : app?.ios_app_link || '';
          const win = window.open(link, '_blank');
          if (win != null) {
            win.focus();
          }
        })
        .catch(error => {});
    }
  };

  return (
    <LandingLayout>
      <div className="hero-home px-4 desk-only-dis">
        <div className="container-fluid px-md-5 hero-content">
          <div className="hero-headline">
            <h1>
              With Enagement App You Can <br />
              <Typer dataText={TEXTS} />
            </h1>
          </div>
          <div className="hero-subheadline">
            The Most Vital Skill Of Any Brand Is Its Ability To Turn There
            Customers Into A Free Sales Force. This Is A Lot Harder Than It
            Sounds...... Until Today
          </div>
          <div className="andr-ios-btn">
            <div className="andr-btn btn">
              <img src={IconAndroid} alt="no_img" /> Android
            </div>
            <div className="ios-btn btn">
              <i className="fab fa-apple" /> IOS
            </div>
          </div>
        </div>
      </div>
      <div
        className="hero-home mob-only-dis"
        style={{ minHeight: window.innerHeight - 244 }}
      >
        <div className="container hero-content">
          <div className="hero-headline">
            <h1>
              Business Will Never <br />
              Be The Same <span className="desk-only-dis">Again</span>
            </h1>
          </div>
          <div className="hero-subheadline">
            The Most Vital Skill Of Any Brand Is Its Ability To Turn There
            Customers Into A Free Sales Force. This Is A Lot Harder Than It
            Sounds...... Until Today
          </div>
          <div className="andr-ios-btn">
            <div className="andr-btn btn">
              <img src={IconAndroid} alt="no_img" /> Android
            </div>
            <div className="ios-btn btn">
              <i className="fab fa-apple" /> IOS
            </div>
          </div>
        </div>
      </div>
      <div className="landing-footer-wrapper">
        <Link to="/registration" className="footer-action">
          Get Started
        </Link>
        <Link to="/login" className="footer-action right">
          Login
        </Link>
        <div className="flex-fill" />
        <div
          className="footer-action"
          onClick={() => onAppInstallerClick(false)}
        >
          <img
            src={require('../assets/images/donwload-from-app-store.svg')}
            alt=""
            className="footer-store-icon"
          />
        </div>
        <div
          className="footer-action"
          onClick={() => onAppInstallerClick(true)}
        >
          <img
            src={require('../assets/images/download-from-play-store.svg')}
            alt=""
            className="footer-store-icon"
          />
        </div>
      </div>
      <div className="container-fluid px-md-5">
        <div className="sec-two px-md-4">
          <div className="mag-content">
            <div className="mag-title">
              <h2>Create magical content</h2>
            </div>
            <div className="mag-text">
              <p>
                Create paid and organic posts for all your social networks in
                one collaborative content calendar. It’s easier to manage
                channels, teams, workflows and campaigns with complete clarity.
                Approval flows and a content pool ensure brand alignment and
                stellar quality.
              </p>
              <a href="/" className="hero-bt">
                See our Publish Content Calendar
              </a>
            </div>
          </div>
          <div className="magic-assets">
            <div className="screenshots-three">
              <div className="image-wrap-magic">
                <img
                  alt=""
                  className="js-lazy-image"
                  src={PublishPostPreviewImg}
                />
              </div>
              <div className="image-wrap-magic">
                <img
                  alt=""
                  className="js-lazy-image"
                  src={PublishStatusPostImg}
                />
              </div>
              <div className="image-wrap-magic">
                <img alt="" className="js-lazy-image" src={PublishNotesImg} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid px-md-5">
        <div className="sec-three px-md-4">
          <div className="moments-assets">
            <div className="screenshots-three">
              <div className="image-wrap-moments">
                <img alt="" className="js-lazy-image" src={EngageHoorayIcon} />
              </div>
              <div className="image-wrap-moments">
                <img alt="" className="js-lazy-image" src={EngageUnreadIcon} />
              </div>
              <div className="image-wrap-moments">
                <img alt="" className="js-lazy-image" src={EngageProfileIcon} />
              </div>
            </div>
          </div>
          <div className="mag-content">
            <div className="mag-title">
              <h2>Manage all channels & moments </h2>
            </div>
            <div className="mag-text">
              <p>
                Take the stress out of community management, engagement and
                customer support. You get one social inbox for all your
                networks, as well as all your customer data in personalized
                profile cards. Engage customers and prospects in the moments
                that count.
              </p>
              <a href="/" className="hero-bt">
                Discover the Engage inbox
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid px-md-5">
        <div className="sec-two px-md-4">
          <div className="mag-content">
            <div className="mag-title">
              <h2>Do wonders with data</h2>
            </div>
            <div className="mag-text">
              <p>
                Power campaigns with more accessible and easier-to-use data. Get
                to know your audiences with social listening while tracking
                content performance. You can also merge your social data with
                your CRM for a unique 360-degree customer view.
              </p>
              <a href="/" className="hero-bt">
                Listen to your audience
              </a>
            </div>
          </div>
          <div className="magic-assets">
            <div className="screenshots-three">
              <div className="image-wrap-magic">
                <img
                  alt=""
                  className="js-lazy-image"
                  src={PublishPostPreviewImg}
                />
              </div>
              <div className="image-wrap-magic">
                <img
                  alt=""
                  className="js-lazy-image"
                  src={PublishStatusPostImg}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="sec-four px-md-4">
        <div className="container-fluid px-md-5">
          <div className="d-flex align-items-start">
            <img src={Chat} alt="no_img" />
            <div className="chat-content">
              <div className="chat-headline w-100">
                <h1>
                  Whatever You`&apos;`re Doing, <br />{' '}
                  <strong>We`&apos;`re Here to Help.</strong>
                </h1>
              </div>
              <div className="chat-subheadline">
                <p>
                  From onboarding, to quick tips and strategic advice, you can
                  count on our support. We offer real-time chat and a host of
                  advisory services to ensure you enjoy the full potential of
                  Falcon.
                </p>
                <a href="/" className="hero-bt">
                  See what our customers think of Falcon
                </a>
              </div>
            </div>
          </div>
          <div className="logos-bottom">
            <div>
              <img alt="" className="js-lazy-image" src={BottomBrand1} />
            </div>
            <div>
              <img alt="" className="js-lazy-image" src={BottomBrand2} />
            </div>
            <div>
              <img alt="" className="js-lazy-image" src={BottomBrand3} />
            </div>
            <div>
              <img alt="" className="js-lazy-image" src={BottomBrand4} />
            </div>
          </div>
        </div>
      </div>
      <div style={{ padding: '6.75rem 0' }}>
        <div className="container">
          <div style={{ padding: '0 11.875rem', textAlign: 'center' }}>
            <h1>
              The <strong>Unified</strong> Social Media &amp; CX Management{' '}
              <strong>Platform.</strong>
            </h1>
            <p>
              Listen, Engage, Publish, Advertise, Measure and build stronger
              connections with your Audience. Falcon is how medium to
              enterprise-sized companies can take social media marketing and
              customer experience to the next level.{' '}
            </p>
            <button type="button" className="hero-bt mt-2">
              Request Demo
            </button>
          </div>
        </div>
      </div>
    </LandingLayout>
  );
};

export default LandingPage;

const TEXTS = [
  { text: 'Turn Customers Into Affiliates', color: '#5ABEA0' },
  { text: 'Create A Compensation Plan', color: '#F7931A' },
  { text: 'Publish Your Oppurtunity', color: '#65A2D9' },
  { text: 'Create Giveaways And Coupons', color: '#F63448' },
];
