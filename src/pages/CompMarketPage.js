import axios from 'axios';
import { useEffect, useState } from 'react';
import CompMarketAddModal from '../components/CompMarketAddModal';
import CompMarketList from '../components/CompMarketList';
import CompMarketSettingsModal from '../components/CompMarketSettingsModal';
import { GX_API_ENDPOINT } from '../configs';
import DashboardLayout from '../layouts/DashboardLayout';
import PopupModalLayout from '../layouts/PopupModalLayout';

const CompMarketPage = () => {
  const [allAppsList, setAllAppsList] = useState([]);
  const [allProductsList, setAllProductsList] = useState([]);
  const CATEGORIES = [
    {
      title: 'CompItems',
      icon: require('../assets/images/comptrollerCategories/comp-item.svg')
        .default,
    },
    {
      title: 'CompLaws',
      icon: require('../assets/images/comptrollerCategories/comp-laws.svg')
        .default,
    },
    {
      title: 'CompBlocks',
      icon: require('../assets/images/comptrollerCategories/comp-blocks.svg')
        .default,
    },
    {
      title: 'CompChains',
      icon: require('../assets/images/comptrollerCategories/comp-chain.svg')
        .default,
    },
    {
      title: 'CompPlans',
      icon: require('../assets/images/comptrollerCategories/comp-plans.svg')
        .default,
      settings: [
        {
          title: 'Products',
          subTitle: 'All Products',
          list: allProductsList,
          queryParam: 'product_id',
        },
        {
          title: 'Apps',
          subTitle: 'All Apps',
          list: allAppsList,
          queryParam: 'app_code',
        },
      ],
    },
  ];
  const [activeCategory, setActiveCategory] = useState(CATEGORIES[0]);
  const [isCategoryPopupOpen, setIsCategoryPopupOpen] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [isSettigsOpen, setIsSettigsOpen] = useState(false);
  const [paramsValue, setParamsValue] = useState('');
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);

  useEffect(() => {
    if (activeCategory) {
      const params = {};

      activeCategory?.settings?.forEach((item) => {
        params[item.queryParam] = 'all';
      });

      setParamsValue(params);
    } else {
      setParamsValue({});
    }
  }, [activeCategory]);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/gxb/product/get`)
      .then(({ data }) => {
        const products = data.products || [];
        const parsed = products.map((item) => ({
          ...item,
          name: item.product_name,
          icon: item.product_icon,
          paramValue: item.product_id,
        }));
        setAllProductsList([
          {
            name: 'All Products',
            icon: require('../assets/images/all-items-icon.svg').default,
            paramValue: 'all',
          },
          ...parsed,
        ]);
      })
      .catch((error) => {
        console.log('Error On Products', error);
      });

    axios
      .get(`${GX_API_ENDPOINT}/gxb/apps/get`)
      .then(({ data }) => {
        const apps = data.apps || [];
        const parsed = apps.map((item) => ({
          ...item,
          name: item.app_name,
          icon: item.app_icon,
          paramValue: item.app_code,
        }));
        setAllAppsList([
          {
            name: 'All Apps',
            icon: require('../assets/images/all-items-icon.svg').default,
            paramValue: 'all',
          },
          ...parsed,
        ]);
      })
      .catch((error) => {
        console.log('Error On Apps', error);
      });
  }, []);

  useEffect(() => {
    setActiveCategory(CATEGORIES[0]);
  }, [allAppsList, allProductsList]);

  useEffect(() => {
    setSearchInput('');
  }, [activeCategory]);

  return (
    <DashboardLayout>
      <div className="landing-child-component comptroller-page-wrapper">
        <div className="header-container">
          <div
            className="active-category"
            onClick={() => setIsCategoryPopupOpen(true)}
          >
            <img
              src={activeCategory.icon}
              alt=""
              className="active-category-icon"
            />
            {activeCategory.title}
          </div>
          <input
            type="text"
            className="search-input"
            placeholder={`Search For ${activeCategory.title}`}
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
          />
          <div
            className="add-button-container"
            onClick={() => setIsAddModalOpen(true)}
          >
            <img
              src={require('../assets/images/plus-icon.svg').default}
              alt=""
              className="add-icon"
            />
          </div>

          <img
            src={require('../assets/images/settings-icon.svg').default}
            alt=""
            className="settings-button"
            onClick={() => setIsSettigsOpen(true)}
          />
        </div>
        <CompMarketList
          activeCategory={activeCategory}
          searchInput={searchInput}
          paramsValue={paramsValue}
        />
      </div>
      <PopupModalLayout
        isOpen={isCategoryPopupOpen}
        onClose={() => setIsCategoryPopupOpen(false)}
        noHeader
        style={{ padding: 0 }}
        width={900}
      >
        <div className="comptroller-category-popup">
          <img
            src={require('../assets/images/copensation-full-logo.svg').default}
            alt=""
            className="header-icon"
          />
          <div className="categories-list">
            {CATEGORIES.map((item) => (
              <div
                key={item.title}
                className={`category-item ${
                  item.title === activeCategory.title ? 'active' : ''
                }`}
                onClick={() => {
                  setActiveCategory(item);
                  setIsCategoryPopupOpen(false);
                }}
              >
                <div className="category-icon-container">
                  <img src={item.icon} alt="" className="category-icon" />
                </div>
                <div className="category-name">{item.title}</div>
              </div>
            ))}
          </div>
        </div>
      </PopupModalLayout>
      <CompMarketSettingsModal
        isOpen={isSettigsOpen}
        onClose={() => setIsSettigsOpen(false)}
        settings={activeCategory.settings || []}
        setParamsValue={setParamsValue}
        paramsValue={paramsValue}
        activeCategory={activeCategory}
      />
      <CompMarketAddModal
        isOpen={isAddModalOpen}
        activeCategory={activeCategory}
        onClose={() => setIsAddModalOpen(false)}
      />
    </DashboardLayout>
  );
};

export default CompMarketPage;
