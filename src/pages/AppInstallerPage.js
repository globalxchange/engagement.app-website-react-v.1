import React from 'react';
import { Link } from 'react-router-dom';
import AppInstaller from '../components/AppInstaller';

const AppInstallerPage = () => (
  <div className="landing-wrapper d-flex flex-column">
    <div className="site-header">
      <div className="app-logo-container">
        <Link to="/mobile-landing">
          <img
            src={require('../assets/images/engagement-app-logo.svg').default}
            alt=""
            className="broker-logo"
          />
        </Link>
      </div>
    </div>
    <div className="container flex-fill d-flex flex-column">
      <AppInstaller />
    </div>
  </div>
);

export default AppInstallerPage;
