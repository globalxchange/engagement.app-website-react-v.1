import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import DashboardTopMenu from '../components/DashboardTopMenu';
import MyProfileCover from '../components/MyProfileCover';
import DashboardLayout from '../layouts/DashboardLayout';

const DashboardHome = () => {
  const [activeNavMenu, setActiveNavMenu] = useState('');
  const [activeBrandCategoriesList, setActiveBrandCategoriesList] = useState();
  const [activeBrandCategory, setActiveBrandCategory] = useState();
  const [showDetails, setShowDetails] = useState();

  useEffect(() => {
    setActiveBrandCategoriesList();
    axios
      .get(
        'https://storeapi.apimachine.com/dynamic/BrokerApp/whatdoyouneedabrokerfor',
        { params: { key: '867e1ca2-8932-4eff-a3b8-e448c936821b' } },
      )
      .then(resp => {
        const { data } = resp;

        // console.log('Data', data);

        if (data.success) {
          setActiveBrandCategoriesList(data.data || []);
          setActiveBrandCategory(data.data[0]);
        } else {
          toast.error('API Error');
        }
      })
      .catch(error => {
        toast.error('Network Error, Please Check Your Internet Connection');
        console.log('Error getting brands list', error);
      });

    return () => {};
  }, [activeNavMenu]);

  return (
    <DashboardLayout>
      <div className="broker-app-main-wrapper">
        <MyProfileCover>
          <DashboardTopMenu
            activeNavMenu={activeNavMenu}
            setActiveNavMenu={setActiveNavMenu}
          />
        </MyProfileCover>
      </div>
    </DashboardLayout>
  );
};

export default DashboardHome;
