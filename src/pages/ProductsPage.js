/* eslint-disable jsx-a11y/no-autofocus */
import React, { useContext, useEffect, useState } from 'react';
import DashboardLayout from '../layouts/DashboardLayout';
import brandsLogo from '../assets/images/gx-brands-logo.svg';
import axios from 'axios';
import ProductsBrandSlider from '../components/ProductsBrandSlider';
import { GX_API_ENDPOINT } from '../configs';
import Skeleton from 'react-loading-skeleton';
import PopupModalLayout from '../layouts/PopupModalLayout';
import ChatContext from '../contexts/ChatContext';

const ProductsPage = () => {
  const { isChatClosed } = useContext(ChatContext);

  const [productCategories, setProductCategories] = useState();
  const [activeCategory, setActiveCategory] = useState('');
  const [allBrands, setAllBrands] = useState();
  const [activeBrand, setActiveBrand] = useState('');
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [allProducts, setAllProducts] = useState('');
  const [filterList, setFilterList] = useState([]);
  const [searchList, setSearchList] = useState([]);
  const [filterPopupOpen, setFilterPopupOpen] = useState(false);
  const [activeFilter, setActiveFilter] = useState(FILTERS[0]);
  const [filterTabList, setFilterTabList] = useState();

  useEffect(() => {
    axios
      .get('https://teller2.apimachine.com/admin/allBankers')
      .then(({ data }) => {
        const brands = [
          { displayName: 'ALL BRANDS', colorCode: '002A51' },
          ...(data?.data || []),
        ];
        setAllBrands(brands);
        setActiveBrand(brands[0] || '');
      })
      .catch(error => {
        console.log('Error on getting brands', error);
      });

    axios
      .get(`${GX_API_ENDPOINT}/gxb/product/category/get`)
      .then(({ data }) => {
        const categories = [{ name: 'All' }, ...(data?.categories || [])];
        setProductCategories(categories);
        // setActiveCategory(categories[0]);
      })
      .catch(error => {
        console.log('Error on getting product categories', error);
      });

    axios
      .get(`${GX_API_ENDPOINT}/gxb/product/get`)
      .then(({ data }) => {
        const products = data?.products || [];
        // console.log('products', products);
        setAllProducts(products);
      })
      .catch(error => {
        console.log('Error on getting products list', error);
      });
  }, []);

  useEffect(() => {
    if (!isSearchOpen) {
      setSearchInput('');
    }
  }, [isSearchOpen]);

  useEffect(() => {
    if (allProducts) {
      const filter = allProducts.filter(x => {
        let mainCategoryFilter = true;

        switch (activeFilter.name) {
          case 'Categories':
            mainCategoryFilter = activeCategory.code
              ? x.product_category_code === activeCategory?.code
              : true;
            break;
          case 'Billing':
            mainCategoryFilter = x[activeCategory?.value];
            break;
          case 'Status':
            mainCategoryFilter = x.product_active === activeCategory?.value;
            break;
          default:
        }

        const brandNameFilter =
          activeBrand?.displayName === 'ALL BRANDS' ||
          x.product_created_by === activeBrand?.email;

        return mainCategoryFilter && brandNameFilter;
      });

      setFilterList(filter);
    }
  }, [allProducts, activeCategory, activeBrand, activeFilter]);

  useEffect(() => {
    const searchQuery = searchInput.trim().toLowerCase();

    const searchFilter = filterList.filter(x =>
      x.product_name.toLowerCase().includes(searchQuery),
    );

    setSearchList(searchFilter);
  }, [filterList, searchInput]);

  useEffect(() => {
    switch (activeFilter.name) {
      case 'Categories':
        setFilterTabList(productCategories);
        if (productCategories) {
          setActiveCategory(productCategories[0]);
        }
        break;
      case 'Billing':
        setFilterTabList(BILLING_FILTER);
        setActiveCategory(BILLING_FILTER[0]);
        break;
      case 'Status':
        setFilterTabList(STATUS_FILTER);
        setActiveCategory(STATUS_FILTER[0]);
        break;
      default:
    }
  }, [activeFilter, productCategories]);

  const onFilterSelect = item => {
    setActiveFilter(item);
    setFilterPopupOpen(false);
  };

  const list = isSearchOpen ? searchList : filterList;

  return (
    <DashboardLayout>
      <div className="broker-app-main-wrapper products-page-wrapper">
        <div className="products-cover-container">
          <img src={brandsLogo} alt="" className="header-img" />
          <div className="cover-header">
            Find The Solution That Works For Your Busines
          </div>
          <ProductsBrandSlider
            activeBrand={activeBrand}
            allBrands={allBrands}
            setActiveBrand={setActiveBrand}
          />
        </div>
        <div className="frag-tab-container">
          {filterTabList ? (
            <>
              {isSearchOpen ? (
                <input
                  type="text"
                  className="search-input"
                  placeholder={`Find Products From ${
                    !activeCategory?.name || activeCategory?.name === 'All'
                      ? 'All Categories'
                      : activeCategory?.name
                  } and ${
                    !activeBrand?.displayName ||
                    activeBrand?.displayName === 'All'
                      ? 'All Brands'
                      : activeBrand?.displayName
                  }...`}
                  autoFocus
                  value={searchInput}
                  onChange={e => setSearchInput(e.target.value)}
                />
              ) : (
                filterTabList?.map(item => (
                  <div
                    key={item.name}
                    className={`tab-item ${
                      item.name === activeCategory?.name ? 'active' : ''
                    }`}
                    onClick={() => setActiveCategory(item)}
                  >
                    {item.name}
                  </div>
                ))
              )}
              <div className="tab-actions-container">
                <div
                  className="tab-action"
                  onClick={() => setFilterPopupOpen(true)}
                >
                  <img
                    src={activeFilter?.icon}
                    alt=""
                    className="tab-action-icon"
                  />
                </div>
                <div
                  className={`tab-action ${isSearchOpen ? 'close' : ''}`}
                  onClick={() => setIsSearchOpen(!isSearchOpen)}
                >
                  <img
                    src={
                      isSearchOpen
                        ? require('../assets/images/close-close.svg').default
                        : require('../assets/images/search-colorful.svg')
                            .default
                    }
                    alt=""
                    className="tab-action-icon"
                  />
                </div>
              </div>
            </>
          ) : (
            Array(5)
              .fill(1)
              .map((_, index) => (
                <div key={index} className="tab-item">
                  <Skeleton count={1} width={100} />
                </div>
              ))
          )}
        </div>
        <div className="frag-container">
          <div className="products-frag">
            {allProducts ? (
              list.length > 0 ? (
                list.map(item => (
                  <div
                    key={item._id}
                    className={`product-item ${
                      !isChatClosed ? 'width-2' : ''
                    } `}
                  >
                    <div className="app-icon-container">
                      <img
                        src={item.product_icon}
                        alt=""
                        className="app-icon"
                      />
                    </div>
                    <div className="app-details">
                      <div className="app-name">{item?.product_name}</div>
                      <div className="app-desc">{item?.sub_text}</div>
                      <div className="action-container">
                        <div className="action">Learn</div>
                        <div className="action">Add</div>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="empty-text">
                  No Products Found For {activeBrand?.displayName}{' '}
                  {!activeCategory?.name || activeCategory?.name === 'All'
                    ? ''
                    : `With ${activeCategory?.name} Category`}
                </div>
              )
            ) : (
              Array(20)
                .fill(1)
                .map((_, index) => (
                  <div key={index} className="product-item">
                    <Skeleton
                      className="app-icon-container"
                      white={100}
                      height={100}
                    />
                    <div className="app-details">
                      <Skeleton width="50%" height={22} />
                      <Skeleton width="70%" height={30} className="app-desc" />
                      <div className="action-container">
                        <Skeleton width={90} height={30} className="action" />
                        <Skeleton width={90} height={30} className="action" />
                      </div>
                    </div>
                  </div>
                ))
            )}
          </div>
        </div>
      </div>
      <PopupModalLayout
        isOpen={filterPopupOpen}
        onClose={() => setFilterPopupOpen(false)}
        headerText="Filter By"
      >
        <div className="filter-container">
          {FILTERS.map(item => (
            <div
              key={item.name}
              className={`filter-item ${
                activeFilter?.name === item.name ? 'active' : ''
              }`}
              onClick={() => onFilterSelect(item)}
            >
              <img src={item.icon} alt="" className="item-icon" />
              <div className="item-name">{item.name}</div>
            </div>
          ))}
        </div>
      </PopupModalLayout>
    </DashboardLayout>
  );
};

export default ProductsPage;

const FILTERS = [
  {
    name: 'Categories',
    icon: require('../assets/images/product-tab-action.svg').default,
  },
  {
    name: 'Billing',
    icon: require('../assets/images/billing-filter-icon.svg').default,
  },
  {
    name: 'Status',
    icon: require('../assets/images/status-filter-icon.svg').default,
  },
];

const STATUS_FILTER = [
  { name: 'In Stock', value: true },
  { name: 'Discontented', value: false },
];

const BILLING_FILTER = [
  { name: 'Monthly', value: 'monthly' },
  { name: 'Annual', value: 'annual' },
  { name: 'One-Time', value: 'lifetime' },
  { name: 'Staking', value: 'staking_allowed' },
];
