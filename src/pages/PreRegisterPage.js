import React from 'react';
import PreRegisterSteps from '../components/PreRegisterSteps';
import { ENGAGEMENT_APP_CODE } from '../configs';
import AuthLayout from '../layouts/AuthLayout';
import AppLogo from '../assets/images/engagement-app-logo.svg';

const PreRegisterPage = () => (
  <AuthLayout>
    <div className="login-page white">
      <div className="d-flex flex-column mx-auto">
        <div className="page-wrapper flex-fill d-flex flex-column justify-content-center my-5">
          <img
            className="broker-logo mx-auto"
            src={AppLogo}
            alt="Broker App™"
          />
          <div className="registration-form-view">
            <PreRegisterSteps appCode={ENGAGEMENT_APP_CODE} />
          </div>
        </div>
      </div>
    </div>
  </AuthLayout>
);

export default PreRegisterPage;
